#![cfg(not(any(test,bench)))]
#![allow(non_upper_case_globals)]
#![ doc = "
# SatIre

> *\"Unsatisfiable here, but maybe satire.\"* [(hint)][lame joke hint]

[lame joke hint]: http://dictionary.reverso.net/french-english/ailleurs (Lame joke hint.)
"]

extern crate time ;

use std::path::Path ;
use std::process::exit ;

use time::SteadyTime ;

use common::SolverRes::{Sat,Unsat,Timeout} ;
use common::Statistics ;


// |===| Top level modules.


/// Macros for logging and asserts.
#[macro_use]
pub mod macros ;
/// Sorted set and map structures.
pub mod sorted ;
/// The git and version info for a build of satire.
pub mod info ;
/// Configuration structure from command line arguments.
pub mod conf ;
/// Basic types, traits, structures and helper functions.
pub mod common ;
/// Statistics structure.
pub mod stats ;
/// Clause structure with two-watched literals.
pub mod clause ;
/// Instance structures with backtracking.
pub mod instance ;
/// Restart strategies.
pub mod restart ;
/// Weight structures for decision strategies and forgetting learnt clauses.
pub mod weight ;
/// Provides traits for a dpll solver.
pub mod dpll ;
/// Implementation of a dpll solver with cdcl by first uip.
pub mod solver ;


/// Entry point.
#[allow(dead_code)]
fn main() {

  // Attempting to parse command line arguments.
  match conf::Conf::new() {

    // Success, running.
    Ok(conf) => {
      for file in conf.files() {
        log!("           |=============================|") ;
        log!("") ;
        run(file, & conf) ;
        log!("") ;
        log!("           |=============================|") ;
        log!("") ;
      }
    },

    // Failure, or help requested.
    // Exit status was set by conf, simply exiting.
    Err(status) => exit(status),
  }
}


/// Runs satire on a file specified by `path`.
#[allow(dead_code)]
fn run(path: & str, conf: & conf::Conf) {
  use dpll::Dpll ;
  use solver::Solver ;

  let path = Path::new( path ) ;

  log!("Running on {}", path.to_str().unwrap()) ;
  log!(nlb, "Parsing... ") ;
  let parse_start = SteadyTime::now() ;
  match Solver::of_path( path, & conf ) {

    Err(s) => {
      log!("") ;
      log!("Error: {}", s) ;
      exit(conf::error_exit_status)
    },

    Ok(mut solver) => {
      let parse_end = SteadyTime::now() ;
      println!("done.") ;

      let status = solver.solve(vec![]) ;
      let end_time = SteadyTime::now() ;
      log!("") ;
      log_res!("{}", status) ;

      let status = match status {
        Sat => {
          if conf.model() {
            log!("") ;
            log!("Model:") ;
            solver.print_valuations("    ") ;
          }

          if conf.model_check() {
            log!("") ;
            if solver.is_model() {
              log!("Model confirmed.") ;
              conf::sat_exit_status
            } else {
              log!("[Error] Model does not satisfy formula.") ;
              if ! conf.model() {
                log!("") ;
                log!("Model:") ;
                solver.print_valuations("    ") ;
              } ;
              conf::error_exit_status
            }
          } else {
            conf::sat_exit_status
          }
        },
        Unsat => conf::unsat_exit_status,
        Timeout => conf::timeout_exit_status,
      } ;

      log!("") ;
      log!("Stats:") ;
      solver.stats().print(end_time - parse_start, parse_end - parse_start) ;

      exit(status)
    },
  }
}
