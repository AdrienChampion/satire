#![cfg(any(test,bench))]
#![allow(non_upper_case_globals)]

extern crate time ;
extern crate rand ;


// |===| Top level modules.


/// Macros for logging and asserts.
#[macro_use]
pub mod macros ;
/// Sorted set and map structures.
pub mod sorted ;
/// The git and version info for a build of satire.
pub mod info ;
/// Configuration structure from command line arguments.
pub mod conf ;
/// Fundamental types, small and helper functions.
pub mod common ;
/// Statistics structure.
pub mod stats ;
/// Clause structure with two-watched literals.
pub mod clause ;
/// Instance structures with backtracking.
pub mod instance ;
/// Restart strategies.
pub mod restart ;
/// Weight structures for decision strategies and forgetting learnt clauses.
pub mod weight ;
/// Provides traits for a dpll solver.
pub mod dpll ;
/// Implementation of a dpll solver with cdcl by first uip.
pub mod solver ;