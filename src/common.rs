#![ doc = "
"]

use std::fmt ;
use std::collections::linked_list::Iter ;
use std::ops ;

use time::Duration ;

use ::sorted::Set ;
use ::clause::Clause ;


/// Type for a variable, used for indexing.
pub type Var = usize ;

/// Type for the size of a cnf and variable count.
pub type Size = usize ;

/// Type for indices.
pub type Index = usize ;

/// A set of variables.
pub type VarSet = Set<Var> ;

/// A set of clauses.
pub type ClauseSet = Set<Index> ;

/// Differentiates between clause indices for the original cnf and learnt
/// clauses.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum CIndex {
  /// Index for a clause from the original cnf.
  Cnf( Index ),
  /// Index for a learnt clause.
  Learnt( Index ),
}

/// A valuation source. Either a decision or a propagation.
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Source {
  /// A decision at some level.
  Decision(Lvl),
  /// A propagation
  Propagation(Lvl, Var, CIndex),
}

impl Source {
  /// The level of a source.
  #[inline]
  pub fn lvl(& self) -> Lvl {
    match * self {
      Source::Decision(lvl) => lvl,
      Source::Propagation(lvl,_,_) => lvl,
    }
  }
}

/// A valuation for a variable.
#[derive(Clone, PartialEq, Debug)]
pub struct Valuation {
  /// The value of the variable.
  pub val: bool,
  /// The source of the valuation.
  pub from: Source
}

impl Valuation {
  /// Returns the decision level of this valuation.
  pub fn lvl(& self) -> Lvl { self.from.lvl() }
}

/// Type for a decision level.
pub type Lvl = usize ;

/// A set of indices.
pub type IndexSet = Set<Index> ;

/// Information for a variable.
#[derive(Clone)]
pub struct VarInfo {
  /// Clauses (non-singleton) of the original cnf the variable appears in.
  pub clauses: Vec<Index>,
  /// Clauses learnt during conflict resolution the variable appears in.
  pub clauses_learnt: IndexSet,
  /// Optional valuation of the variable.
  pub value: Option<Valuation>,
  /// Value for the variable satisfying the most clauses in the orginial cnf.
  pub hint: bool,
  /// Weight of the variable.
  pub weight: ::weight::Weight,
  /// Number of kids of this node in the conflict graph during conflict
  /// resolution.
  pub conflict_kids: usize,
}

impl VarInfo {
  /// Returns an iterator on all the clauses in a var info. Learnt clauses are
  /// first.
  pub fn clause_iter<'a>(& 'a self) -> ClauseIter<'a> {
    ClauseIter::new( & self.clauses, & self.clauses_learnt )
  }
  /// Adds a learnt clause to a var info.
  pub fn add_learnt(& mut self, i: Index) {
    self.clauses_learnt.add(i)
  }
  /// Removes a learnt clause from a var info.
  pub fn rm_learnt(& mut self, i: Index) {
    self.clauses_learnt.rm(i) ;
  }
}


/// Structure for  an iterator on the clauses of a var info.
pub struct ClauseIter<'a> {
  /// The index of the current clause in `clauses`.
  index: usize,
  /// The clauses of the cnf from the var info.
  clauses: & 'a Vec<Index>,
  /// An iterator on the learnt clauses of the var info.
  clauses_learnt: Iter<'a, Index>,
}

impl<'a> ClauseIter<'a> {
  /// Creates a new iterator on a vector of clauses and a set of learnt
  /// clauses.
  pub fn new(
    clauses: & 'a Vec<Index>, clauses_learnt: & 'a IndexSet
  ) -> ClauseIter<'a> {
    ClauseIter {
      index: 0, clauses: clauses, clauses_learnt: clauses_learnt.iter()
    }
  }
}

/// Iterates on the learnt clauses first, then on the regular clauses.
impl<'a> Iterator for ClauseIter<'a> {
  type Item = CIndex ;
  fn next(& mut self) -> Option<CIndex> {
    if let Some(i) = self.clauses_learnt.next() {
      return Some( CIndex::Learnt(*i) )
    } ;
    if self.index < self.clauses.len() {
      let res = Some(
        CIndex::Cnf(self.clauses[self.index])
      ) ;
      self.index += 1 ;
      res
    } else { None }
  }
}

/// Mapping from variables to their information.
pub struct VarMap {
  /// The map itself. Info at `0` is dummy for index / var consistency.
  map: Vec<VarInfo>
}

impl VarMap {
  /// Creates a new variable map.
  pub fn new(map: Vec<VarInfo>) -> VarMap { VarMap { map: map } }

  /// Length of the map, equal to the index of the the highest variable plus
  /// one.
  pub fn len(& self) -> usize { self.map.len() }

  /// Undoes all valuations of decisions of level higher than `lvl`. Returns
  /// the decision variable corresponding to `lvl`.
  pub fn backjump(& mut self, lvl: Lvl) -> ::weight::WeightVarMap {
    use ::weight::* ;
    let mut weight_map = WeightVarMap::new(Weight::pivot()) ;
    let pivot = self.map.len() / 2 ;
    for var in 1..self.map.len() {
      self.map[var].conflict_kids = 0 ;
      if let VarInfo { value: Some(ref v), ref weight, .. } = self.map[var] {
        if v.lvl() <= lvl { continue } else {
          weight_map.update(pivot, *weight, var) ;
        }
      } else { continue } ;
      self.map[var].value = None
    } ;
    weight_map
  }

  /// The value of a literal in a variable map.
  pub fn val_of_lit(& self, lit: Lit) -> Option<bool> {
    if lit.l > 0 {
      match self.map[ lit.l as Var ].value {
        Some(Valuation { val, .. }) => Some(val),
        None => None,
      }
    } else {
      match self.map[ (- lit.l) as Var ].value {
        Some(Valuation { val, .. }) => Some(!val),
        None => None,
      }
    }
  }

  /// Sets the value of a variable so that a literal is true.
  pub fn set_val_of_lit(& mut self, lit: Lit, from: Source) {
    if lit.l > 0 {
      self.map[lit.l as Var].value = Some(
        Valuation { val: true, from: from }
      )
    } else {
      self.map[(- lit.l) as Var].value = Some(
        Valuation { val: false, from: from }
      )
    }
  }
}

impl ops::Index<Var> for VarMap {
  type Output = VarInfo ;
  fn index(& self, i: Var) -> & VarInfo {
    & self.map[i]
  }
}
impl ops::IndexMut<Var> for VarMap {
  fn index_mut(& mut self, i: Var) -> & mut VarInfo {
    & mut self.map[i]
  }
}


/// A literal is a wrapper around a `isize`.
#[derive(PartialEq,Copy,Clone,Debug,Eq,PartialOrd,Ord)]
pub struct Lit {
  /// The actual literal.
  pub l: isize
}

impl Lit {
  /// Converts a variable to a literal.
  #[inline]
  pub fn of_var(var: Var) -> Lit { Lit { l: var as isize } }

  /// Converts a literal to a variable (`abs` then cast).
  #[inline]
  pub fn to_var(& self) -> Var { self.l.abs() as Var }

  /// Negates a literal.
  #[inline]
  pub fn neg(& self) -> Lit { Lit { l: - self.l } }

  /// Returns the literal corresponding to the valuation of a variable.
  #[inline]
  pub fn of_val(var: Var, val: & Valuation) -> Lit {
    if val.val { Lit { l: var as isize } }
    else { Lit { l: -(var as isize) } }
  }

  /// Returns the literal corresponding to the negation of a valuation of a
  /// variable. Used in lemma construction during conflict resolution.
  #[inline]
  pub fn neg_of_val(var: Var, val: & Valuation) -> Lit {
    if val.val { Lit { l: -(var as isize) } }
    else { Lit { l: var as isize } }
  }

  /// Returns the valuation making a literal true.
  #[inline]
  pub fn to_val(& self, from: Source) -> (Var, Valuation) {
    if self.l >= 0 {
      ( self.l as Var, Valuation { val: true, from: from } )
    } else {
      ( (- self.l) as Var, Valuation { val: false, from: from } )
    }
  }
  
}



/// Type for the status of a clause.
#[derive(PartialEq, Debug)]
pub enum CStatus {
  /// Clause is unsat.
  Unsat,
  /// Clause is unit.
  Unit(Lit),
  /// Clause is sat.
  Sat,
  /// Clause is undef.
  Undef,
}





/// Type of a cnf formula.
pub type Cnf = Vec<Clause> ;


/// Type for the status of a CNF.
#[derive(PartialEq, Debug)]
pub enum CnfStatus {
  /// CNF has a conflict for a conjuncts.
  Conflict(CIndex),
  /// CNF is sat.
  Sat,
  /// CNF is undef.
  Undef,
}




/// Type for the result returned by a solver.
#[derive(PartialEq, Debug, Copy, Clone)]
pub enum SolverRes {
  /// Instance is sat.
  Sat,
  /// Instance is unsat.
  Unsat,
  /// Solver reached timeout.
  Timeout,
}




/// Keeps tracks of the statistics in an instance.
pub trait Statistics {
  /// Number of decisions made.
  fn decisions(& self) -> usize ;
  /// Number of assignments made.
  fn assignments(& self) -> usize ;
  /// Number of conflicts encountered.
  fn conflicts(& self) -> usize ;
  /// Number of restarts encountered.
  fn restarts(& self) -> usize ;
  /// Time spent resolving.
  fn resolution_time(& self) -> Duration ;
  /// Time spent backjumping.
  fn backjump_time(& self) -> Duration ;
  /// Time spent propagating (includes time updating clauses).
  fn propagation_time(& self) -> Duration ;
  /// Time spent making decisions.
  fn decision_time(& self) -> Duration ;
  /// Time since the beginning of the analysis (is zero until timer is
  /// stopped).
  fn total_time(& self) -> Duration ;
  /// Time between now and the beginning of the analysis.
  fn current_time(& self) -> Duration ;
  /// Returns true iff the timeout has been reached.
  #[inline]
  fn timeout_reached(& self) -> bool ;

  /// Prints the statistics in a nice format.
  fn print(
    & self, total: Duration, parsing: Duration
  ) {
    use std::cmp::max ;
    log!(
      "{:>13} ms total",
      commate( millis(total) )
    ) ;
    log!(
      "{:>13} ms > parsing",
      commate( millis(parsing) )
    ) ;
    log!(
      "{:>13} ms > solving",
      commate( millis(self.total_time()) )
    ) ;
    log!(
      "{:>13} ms   > deciding",
      commate( millis(self.decision_time()) )
    ) ;
    log!(
      "{:>13} ms   > propagating",
      commate( millis(self.propagation_time()) )
    ) ;
    log!(
      "{:>13} ms   > resolving",
      commate( millis(self.resolution_time()) )
    ) ;
    log!(
      "{:>13} ms   > backjumping",
      commate( millis(self.backjump_time()) )
    ) ;
    let total_time = max(millis(self.total_time()), (1 as i64)) as f64 ;
    log!(
      "{:>13}    conflicts    ({:>9}/s)",
      commate( self.conflicts() ),
      commate( (1000f64 * (self.conflicts() as f64) / total_time) as usize ),
    ) ;
    log!(
      "{:>13}    restarts     ({:>9}/s)",
      commate( self.restarts() ),
      commate( (1000f64 * (self.restarts() as f64) / total_time) as usize ),
    ) ;
    log!(
      "{:>13}    propagations ({:>9}/s)",
      commate( self.assignments() ),
      commate( (1000f64 * (
        (self.assignments() - self.decisions()) as f64
      ) / total_time) as usize ),
    ) ;
    log!(
      "{:>13}    decisions    ({:>9}/s)",
      commate( self.decisions() ),
      commate( (1000f64 * (self.decisions() as f64) / total_time) as usize ),
    ) ;
  }
}



// |===| Helper things.


fn millis(duration: Duration) -> i64 {
  duration.num_milliseconds()
}

fn commate_str(s: String) -> String {
  let chars = s.chars() ;
  let mut i = 0 ;
  let len = s.len() ;
  let mut res = String::with_capacity(len) ;
  for c in chars {
    res.push(c) ;
    if i != len - 1 && (len - (i + 1)) % 3 == 0 {
      res.push(',')
    } ;
    i += 1
  } ;
  res
}

fn commate<T: fmt::Display>(t: T) -> String {
  commate_str( format!("{}", t) )
}




// |===| Display things.


static sat_str:         & 'static str = "sat"      ;
static unit_str:        & 'static str = "unit"     ;
static undef_str:       & 'static str = "undef"    ;
static unsat_str:       & 'static str = "unsat"    ;
static conflict_str:    & 'static str = "conflict" ;
static sat_res_str:     & 'static str = "SAT"      ;
static unsat_res_str:   & 'static str = "UNSAT"    ;
static timeout_res_str: & 'static str = "TIMEOUT"  ;


impl fmt::Display for Source {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match * self {
      Source::Decision(lvl) => write!(
        fmt, "@{}", lvl
      ),
      Source::Propagation(lvl,v,c) => write!(
        fmt, "@{}|{}|{}", lvl, v, c
      ),
    }
  }
}


impl fmt::Display for CIndex {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match * self {
      CIndex::Cnf(ref i) => write!(fmt, "{}c", i),
      CIndex::Learnt(ref i) => write!(fmt, "{}cl", i),
    }
  }
}


impl fmt::Display for Valuation {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    write!(fmt, "{}{}", if self.val {"+"} else {"-"}, self.from)
  }
}


impl fmt::Display for VarInfo {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match self.value {
      None => write!(fmt, "?"),
      Some(ref val) => write!(fmt, "{}", val),
    }
  }
}

impl fmt::Display for VarMap {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    let mut map = self.map.iter() ;
    // Skipping `0` since it's dummy.
    map.next() ;

    if let Some(info) = map.next() {
      let mut cpt = 1 ;
      try!( write!(fmt, "{}{}", cpt, info) ) ;
      for info in map {
        cpt += 1 ;
        try!( write!(fmt, ", {}{}", cpt, info) )
      }
    } ;

    Ok(())
  }
}

impl fmt::Display for Lit {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    write!(fmt, "{}", self.l)
  }
}


impl fmt::Display for CStatus {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match *self {
      CStatus::Unit(lit) => write!(fmt, "{}({})", unit_str, lit),
      CStatus::Unsat => write!(fmt, "{}", unsat_str),
      CStatus::Sat => write!(fmt, "{}", sat_str),
      CStatus::Undef => write!(fmt, "{}", undef_str),
    }
  }
}


impl fmt::Display for CnfStatus {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match *self {
      CnfStatus::Conflict(clause) =>
        write!(fmt, "{}({})", conflict_str, clause),
      CnfStatus::Sat => write!(fmt, "{}", sat_str),
      CnfStatus::Undef => write!(fmt, "{}", undef_str),
    }
  }
}


impl fmt::Display for SolverRes {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    match *self {
      SolverRes::Unsat => write!(fmt, "{}", unsat_res_str),
      SolverRes::Sat => write!(fmt, "{}", sat_res_str),
      SolverRes::Timeout => write!(fmt, "{}", timeout_res_str),
    }
  }
}

// |===| Test things.

#[cfg(test)]
use rand::{Rand, StdRng} ;

#[cfg(test)]
fn get(rng: & mut StdRng) -> usize {
  u32::rand(rng) as usize
}

#[test]
fn var_map() {
  use self::VarInfo ;
  use ::weight::Weight ;

  let vec = vec![
    (VarInfo {
      clauses: vec![], clauses_learnt: IndexSet::new(0),
      value: None, hint: true, weight: Weight::new(), conflict_kids: 0
    }),
    (VarInfo {
      clauses: vec![2, 3], clauses_learnt: IndexSet::new(0),
      value: None, hint: true, weight: Weight::new(), conflict_kids: 0
    }),
    (VarInfo {
      clauses: vec![1, 4], clauses_learnt: IndexSet::new(0),
      value: None, hint: true, weight: Weight::new(), conflict_kids: 0
    }),
    (VarInfo {
      clauses: vec![2, 5], clauses_learnt: IndexSet::new(0),
      value: None, hint: true, weight: Weight::new(), conflict_kids: 0
    }),
    (VarInfo {
      clauses: vec![4, 3], clauses_learnt: IndexSet::new(0),
      value: None, hint: true, weight: Weight::new(), conflict_kids: 0
    }),
  ] ;

  let lits = vec![
    Lit::of_var(1).neg(),
    Lit::of_var(2),
    Lit::of_var(3),
    Lit::of_var(4).neg(),
    Lit::of_var(1),
    Lit::of_var(2).neg(),
    Lit::of_var(3).neg(),
    Lit::of_var(4)
  ] ;

  let map = vec.clone() ;
  let mut map = VarMap { map: map } ;

  assert!( map.len() == vec.len() ) ;
  for lit in 0..lits.len() {
    let lit = lits[lit] ;
    assert_eq!( map.val_of_lit(lit), None ) ;
  } ;
  for lit in 0..lits.len() {
    let lit = lits[lit] ;
    let from = Source::Decision(0) ;
    map.set_val_of_lit(lit, from) ;
    assert_eq!( map.val_of_lit(lit), Some( true ) ) ;
  } ;
  for lit in 0..lits.len() {
    let lit = lits[lit] ;
    let from = Source::Decision(0) ;
    let (var,val) = lit.to_val(from) ;
    let res = lit.l > 0 ;
    map[var].value = Some(val) ;
    assert_eq!(
      match map[var].value {
        Some( Valuation { val, .. } ) => val,
        None => panic!("unreachable"),
      },
      res
    ) ;
    let from = Source::Decision(0) ;
    let (var,val) = lit.neg().to_val(from) ;
    map[var].value = Some(val) ;
    assert_eq!(
      match map[var].value {
        Some( Valuation { val, .. } ) => val,
        None => panic!("unreachable"),
      },
      ! res
    ) ;
  } ;
}

#[test]
fn lit_var() {
  use std::mem::size_of ;

  // Mem size check.
  assert_eq!( size_of::<Lit>(), size_of::<isize>() ) ;

  fn test(rng: & mut StdRng) {
    let var = get(rng) ;
    let lit = Lit::of_var(var) ;
    let lit_neg = lit.neg() ;
    // Variable checks.
    assert_eq!( lit.l, var as isize ) ;
    assert_eq!( lit.to_var(), var ) ;
    assert_eq!( lit_neg.l, - (var as isize) ) ;
    assert_eq!( lit_neg.to_var(), var ) ;

    // Valuation checks.
    let from = Source::Decision(get(rng)) ;
    let val = Valuation { val: false, from: from } ;
    let neg_val = Valuation { val: true, from: from } ;
    let var = get(rng) ;
    let lit = Lit::of_val(var, & val) ;
    let neg_lit = Lit::of_val(var, & neg_val) ;
    assert_eq!( lit.l, - (var as isize) ) ;
    assert_eq!( neg_lit.l, var as isize ) ;
    assert_eq!( lit.to_val(from), (var, val.clone()) ) ;
    assert_eq!(
      Lit::neg_of_val(var, & val).to_val(from), (var, neg_val.clone())
    ) ;
    assert_eq!(
      Lit::neg_of_val(var, & neg_val).to_val(from), (var, val.clone())
    ) ;
  }

  let mut rng = match StdRng::new() {
    Ok(rng) => rng,
    Err(s) => panic!("Could not create rng: {}", s),
  } ;

  for _ in 0..200 {
    test(& mut rng)
  }
}

