#![ doc = "
"]

use std::collections::LinkedList ;
use std::collections::linked_list::Iter ;
use std::cmp::Ord ;
use std::fmt ;

use ::common::Var ;
use ::weight::Weight ;


macro_rules! do_forward_mut {
  (fn $name:ident (
    $slf:ident, $cmp:ident : $cmp_t:ty, $( $params:ident : $param_ts:ty ),*
  ) -> $t:ty {
    $e:ident, $old:ident =>
      eq $eq_blk:block gt $gt_blk:block else $end_blk:block
  } ) => (
    #[allow(unused_mut)]
    fn $name(
      & mut $slf, $cmp: $cmp_t, $($params: $params_ts),*
    ) -> $t {
      let mut $old = LinkedList::new() ;
      $old.append(& mut $slf.list) ;
      loop {
        if let Some(mut $e) = $old.pop_front() {
          if $e < $cmp { $slf.list.push_back($e) }
          else {
            if $e == $cmp $eq_blk else $gt_blk
          }
        } else $end_blk
      }
    }
  ) ;
  (fn $name:ident (
    $slf:ident, $cmp:ident : $cmp_t:ty
  ) -> $t:ty {
    $e:ident, $old:ident =>
      eq $eq_blk:block gt $gt_blk:block else $end_blk:block
  } ) => (
    do_forward_mut!(fn $name($slf,$cmp:$cmp_t,) -> $t {
      $e,$old => eq $eq_blk gt $gt_blk else $end_blk
    }) ;
  ) ;
}

macro_rules! do_backward_mut {
  (fn $name:ident (
    $slf:ident, $cmp:ident : $cmp_t:ty, $( $params:ident : $param_ts:ty ),*
  ) -> $t:ty {
    $e:ident, $new:ident =>
      eq $eq_blk:block lt $lt_blk:block else $end_blk:block
  } ) => (  
    #[allow(unused_mut)]
    fn $name(
      & mut $slf, $cmp: $cmp_t, $($params: $params_ts),*
    ) -> $t {
      let mut $new = LinkedList::new() ;
      loop {
        if let Some(mut $e) = $slf.list.pop_back() {
          if $e > $cmp { $new.push_front($e) }
          else {
            if $e == $cmp $eq_blk else $lt_blk
          }
        } else $end_blk
      }
    }
  ) ;
  (fn $name:ident (
    $slf:ident, $cmp:ident : $cmp_t:ty
  ) -> $t:ty {
    $e:ident, $new:ident =>
      eq $eq_blk:block lt $lt_blk:block else $end_blk:block
  } ) => (
    do_backward_mut!(fn $name($slf,$cmp:$cmp_t,) -> $t {
      $e,$new => eq $eq_blk lt $lt_blk else $end_blk
    }) ;
  ) ;
}



/// A wrapper around a sorted `std::collections::LinkedList` with set-like
/// behavior.
///
/// The list is kept sorted. The pivot supplied when creating a set is used
/// when adding / removing to decide in which direction the list is traversed
/// to find the relevant position.
pub struct Set<T: Ord + Sized> {
  /// The internal linked list.
  list: LinkedList<T>,
  /// The pivot value to decide how to traverse the list.
  pivot: T,
}

impl<T: Ord + Copy> Set<T> {

  /// Creates a new, empty set.
  pub fn new(pivot: T) -> Self {
    Set { list: LinkedList::new(), pivot: pivot }
  }

  /// Creates a new set from a vector.
  pub fn of_vec(pivot: T, v: Vec<T>) -> Self {
    let mut set = Set::new(pivot) ;
    for v in v.into_iter() {
      set.add(v)
    }
    set
  }

  /// Returns `true` iff the element is in the set.
  pub fn mem(& self, element: T) -> bool {
    if element <= self.pivot {
      for e in self.iter() {
        if element > * e { continue } else {
          if element == * e { return true }
          else { return false }
        }
      } ;
      return false
    } else {
      for e in self.iter().rev() {
        if element < * e { () }
        else {
          if element == * e { return true }
          else { return false }
        }
      } ;
      return false
    }
  }

  /// Returns the first element of the list, if any, but does not pop it.
  pub fn head(& self) -> Option<& T> {
    if let Some(head) = self.list.front() { Some(& head) }
    else { None }
  }

  /// Pops the first element of the list, if any.
  pub fn pop(& mut self) -> Option<T> { self.list.pop_front() }

  /// Minimal element of the set.
  pub fn min(& self) -> Option<& T> { self.list.front() }

  /// Maximal element of the set.
  pub fn max(& self) -> Option<& T> { self.list.back() }

  /// Returns the length of the list (`O(1)`).
  #[inline]
  pub fn len(& self) -> usize { self.list.len() }
  /// Returns an iterator on the pairs of the list.
  #[inline]
  pub fn iter(& self) -> Iter<T> { self.list.iter() }
  /// Clears the list.
  #[inline]
  pub fn clear(& mut self) { self.list.clear() }
  /// Returns true iff the list is empty.
  #[inline]
  pub fn is_empty(& self) -> bool { self.list.is_empty() }

  /// Returns true iff the list is sorted. Should **always** be true.
  pub fn is_sorted(& self) -> bool {
    let mut iter = self.iter() ;
    if let Some(mut prev) = iter.next() {
      for e in iter {
        if prev > e { return false }
        prev = e
      } ;
      true
    } else { true }
  }


  /// Inserts an element in the sorted list. Goes forward if `e <= pivot` and
  /// backward otherwise, where `pivot` is the value the set was created with.
  pub fn add(& mut self, e: T) {
    if e <= self.pivot { self.add_for(e) } else { self.add_bak(e) }
  }

  /// Inserts an element in the sorted list. Goes forward.
  do_forward_mut!(
    fn add_for(self, element: T) -> () {
      e, old => eq {
        self.list.push_back(e) ;
        self.list.append(& mut old) ;
        break
      } gt {
        self.list.push_back(element) ;
        self.list.push_back(e) ;
        self.list.append(& mut old) ;
        break
      } else {
        self.list.push_back(element) ;
        break
      }
    }
  ) ;
  #[cfg(any(test, bench))]
  pub fn test_add_for(& mut self, element: T) { self.add_for(element) }

  /// Inserts an element in the sorted list. Goes backward.
  do_backward_mut!(
    fn add_bak(self, element: T) -> () {
      e, new => eq {
        self.list.push_back(e) ;
        self.list.append(& mut new) ;
        break
      } lt {
        new.push_front(element) ;
        new.push_front(e) ;
        self.list.append(& mut new) ;
        break
      } else {
        new.push_front(element) ;
        self.list.append(& mut new) ;
        break
      }
    }
  ) ;
  #[cfg(any(test, bench))]
  pub fn test_add_bak(& mut self, element: T) { self.add_bak(element) }


  /// Removes an element from the sorted list. Goes forward if `e <= pivot` and
  /// backward otherwise, where `pivot` is the value the set was created with.
  pub fn rm(& mut self, e: T) -> bool {
    if e <= self.pivot { self.rm_for(e) } else { self.rm_bak(e) }
  }

  /// Removes an element from the sorted list. Goes forward.
  do_forward_mut!(
    fn rm_for(self, element: T) -> bool {
      e, old => eq {
        self.list.append(& mut old) ;
        return true
      } gt {
        self.list.push_back(e) ;
        self.list.append(& mut old) ;
        return false
      } else { return false }
    }
  ) ;
  #[cfg(any(test, bench))]
  pub fn test_rm_for(& mut self, element: T) { self.rm_for(element) ; }

  /// Removes an element from the sorted list. Goes backward.
  do_backward_mut!(
    fn rm_bak(self, element: T) -> bool {
      e, new => eq {
        self.list.append(& mut new) ;
        return true
      } lt {
        self.list.push_back(e) ;
        self.list.append(& mut new) ;
        return false
      } else {
        self.list.append(& mut new) ;
        return false
      }
    }
  ) ;
  #[cfg(any(test, bench))]
  pub fn test_rm_bak(& mut self, element: T) { self.rm_bak(element) ; }


  /// Adds the element of `rhs` to `self`. Traverses forward of backward
  /// depending on the value of the min of `rhs` w.r.t. the pivot of `self`.
  pub fn union(& mut self, rhs: & mut Self) {
    let min = if let Some( min ) = rhs.min() {
      * min
    } else {
      return ()
    } ;
    if min >= self.pivot {
      // println!("union bak") ;
      self.union_bak(rhs)
    } else {
      // println!("union for") ;
      self.union_for(rhs)
    }
  }

  fn union_for(& mut self, rhs: & mut Self) {
    let rhs = & mut rhs.list ;
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    'top: loop {
      // Retrieving element to add.
      if let Some( to_add ) = rhs.pop_front() {
        loop {
          if let Some( next ) = old.pop_front() {
            if next < to_add {
              // Need to pop more.
              self.list.push_back(next)
            } else {
              if next == to_add {
                // Element is already there.
                self.list.push_back(next) ;
                // Moving on to next element.
                break
              } else {
                // Element is not already there.
                self.list.push_back(to_add) ;
                // Putting `next` back in the old list for next element.
                old.push_front(next) ;
                break
              }
            }
          } else {
            // Lhs has no more elements, appending rhs.
            self.list.push_back(to_add) ;
            self.list.append(rhs) ;
            break 'top
          }
        }
      } else {
        // No more elements to add.
        self.list.append(& mut old) ;
        break
      }
    }
  }


  fn union_bak(& mut self, rhs: & mut Self) {
    let rhs = & mut rhs.list ;
    let mut new = LinkedList::new() ;
    'top: loop {
      // Retrieving element to add.
      if let Some( to_add ) = rhs.pop_back() {
        loop {
          if let Some( next ) = self.list.pop_back() {
            if next > to_add {
              // Need to pop more.
              new.push_front(next)
            } else {
              if next == to_add {
                // Element is already there.
                new.push_front(next) ;
                // Moving on to next element.
                break
              } else {
                // Element is not already there.
                new.push_front(to_add) ;
                // Putting `next` back in the list for next element.
                self.list.push_back(next) ;
                break
              }
            }
          } else {
            // Lhs has no more elements, appending rhs.
            self.list.append(rhs) ;
            self.list.push_back(to_add) ;
            // Adding the new list at the end.
            self.list.append(& mut new) ;
            break 'top
          }
        }
      } else {
        // No more elements to add.
        self.list.append(& mut new) ;
        break
      }
    }
  }


  /// Removes the element of `rhs` from `self`. Traverses forward of backward
  /// depending on the value of the min of `rhs` w.r.t. the pivot of `self`.
  pub fn minus(& mut self, rhs: & Self) {
    let min = if let Some( min ) = rhs.min() {
      * min
    } else {
      return ()
    } ;
    if min >= self.pivot {
      // println!("minus bak") ;
      self.minus_bak(rhs)
    } else {
      // println!("minus for") ;
      self.minus_for(rhs)
    }
  }

  fn minus_for(& mut self, rhs: & Self) {
    let mut rhs = rhs.list.iter() ;
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    'top: loop {
      // Retrieving element to remove.
      if let Some( to_rm ) = rhs.next() {
        let to_rm = * to_rm ;
        loop {
          if let Some( next ) = old.pop_front() {
            if next < to_rm {
              // Need to pop more.
              self.list.push_back(next)
            } else {
              if next == to_rm {
                // Moving on to next element.
                break
              } else {
                // Element is not there.
                // Putting `next` back in the old list for next element.
                old.push_front(next) ;
                break
              }
            }
          } else {
            // Lhs has no more elements, done.
            break 'top
          }
        }
      } else {
        // No more elements to remove.
        self.list.append(& mut old) ;
        break
      }
    }
  }


  fn minus_bak(& mut self, rhs: & Self) {
    let mut rhs = rhs.list.iter().rev() ;
    let mut new = LinkedList::new() ;
    'top: loop {
      // Retrieving element to remove.
      if let Some( to_rm ) = rhs.next() {
        let to_rm = * to_rm ;
        loop {
          if let Some( next ) = self.list.pop_back() {
            if next > to_rm {
              // Need to pop more.
              new.push_front(next)
            } else {
              if next == to_rm {
                // Moving on to next element.
                break
              } else {
                // Putting `next` back in the list for next element.
                self.list.push_back(next) ;
                break
              }
            }
          } else {
            // No more elements in lhs, adding the new list at the end.
            self.list.append(& mut new) ;
            break 'top
          }
        }
      } else {
        // No more elements to remove.
        self.list.append(& mut new) ;
        break
      }
    }
  }

}

impl<T: Ord + Copy> Clone for Set<T> {
  fn clone(& self) -> Self {
    let mut list = LinkedList::new() ;
    for e in self.iter() {
      list.push_back(* e) ;
    } ;
    Set { list: list, pivot: self.pivot }
  }
}

impl<T: Ord + Copy + fmt::Debug> PartialEq for Set<T> {
  fn eq(& self, rhs: & Self) -> bool {
    let len = self.len() ;
    if len != rhs.len() { false }
    else {
      let (mut iter1, mut iter2) = (self.iter(), rhs.iter()) ;
      for _ in 0..len {
        match (iter1.next(), iter2.next()) {
          (Some(e1), Some(e2)) =>
            if e1 != e2 { return false },
          (n1, n2) =>
            panic!("unreachable: {:?} / {:?}", n1, n2),
        }
      } ;
      true
    }
  }
}



impl<
  T: Ord + Copy + fmt::Display
> fmt::Display for Set<T> {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    try!( write!(fmt, "{{") ) ;
    let mut is_first = true ;
    for elmt in self.iter() {
      if is_first { is_first = false }
      else { try!( write!(fmt, ", ") ) }
      try!( write!(fmt, "{}", elmt) ) ;
    } ;
    write!(fmt, "}}")
  }
}


impl<
  T: Ord + Copy + fmt::Display
> fmt::Debug for Set<T> {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    try!( write!(fmt, "{{") ) ;
    let mut is_first = true ;
    for elmt in self.iter() {
      if is_first { is_first = false }
      else { try!( write!(fmt, ", ") ) }
      try!( write!(fmt, "{}", elmt) ) ;
    } ;
    write!(fmt, "}}")
  }
}




/// A wrapper around a sorted `std::collections::LinkedList` with map-like
/// behavior.
///
/// The list is kept sorted. The pivot supplied when creating a set is used
/// when adding / removing / updating to decide in which direction the list is
/// traversed to find the relevant position.
pub struct Map<Key: Ord + Sized, Val> {
  /// The internal linked list.
  list: LinkedList<(Key,Val)>,
  /// The pivot value to decide how to traverse the list.
  pivot: Key,
}

impl<Key: Ord + Sized + fmt::Display, Val: Sized> Map<Key, Val> {

  /// Creates a new, empty set.
  pub fn new(pivot: Key) -> Map<Key, Val> {
    Map { list: LinkedList::new(), pivot: pivot }
  }

  /// Returns the length of the list (`O(1)`).
  #[inline]
  pub fn len(& self) -> usize { self.list.len() }
  /// Returns an iterator on the list.
  #[inline]
  pub fn iter(& self) -> Iter<(Key,Val)> { self.list.iter() }
  /// Clears the list.
  #[inline]
  pub fn clear(& mut self) { self.list.clear() }
  /// Returns true iff the list is empty.
  #[inline]
  pub fn is_empty(& self) -> bool { self.list.is_empty() }

  /// Returns true iff the list is sorted. Should **always** be true.
  pub fn is_sorted(& self) -> bool {
    let mut iter = self.iter() ;
    if let Some(pair) = iter.next() {
      let (ref key, _) = * pair ;
      let mut key = key ;
      for pair in iter {
        let (ref k, _) = * pair ;
        if key > k { return false }
        key = k
      } ;
      true
    } else { true }
  }

  /// Returns the value a key is mapped to in a map.
  pub fn get(& self, key: Key) -> Option<& Val> {
    if key <= self.pivot {
      for pair in self.iter() {
        let (ref k, ref v) = * pair ;
        if k < & key { () } else {
          if k == & key { return Some(v) }
          else { return None }
        }
      } ;
      return None
    } else {
      for pair in self.iter().rev() {
        let (ref k, ref v) = * pair ;
        if k > & key { () } else {
          if k == & key { return Some(v) }
          else { return None }
        }
      } ;
      return None
    }
  }


  /// Updates the value of a key in a map if it's there, adds something
  /// otherwise.
  pub fn update_funs<U, A> (& mut self, key: Key, update: U, add: A) -> bool
  where U: Fn(& mut Val), A: Fn() -> Val {
    if key <= self.pivot { self.update_for(key,update,add) }
    else { self.update_bak(key,update,add) }
  }

  /// Forward version of `update_funs`.
  fn update_for<U, A> (& mut self, key: Key, update: U, add: A) -> bool
  where U: Fn(& mut Val), A: Fn() -> Val {
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    loop {
      if let Some( (k, mut v) ) = old.pop_front() {
        if k < key { self.list.push_back( (k,v) ) }
        else {
          if k == key {
            // Update binding.
            update(& mut v) ;
            self.list.push_back( (key, v) ) ;
            // Append tail.
            self.list.append( & mut old ) ;
            // Done.
            return true
          } else {
            // Add new binding.
            self.list.push_back( (key, add()) ) ;
            // Add current binding.
            self.list.push_back( (k,v) ) ;
            // Append tail.
            self.list.append( & mut old ) ;
            // Done.
            return false
          }
        }
      } else {
        // Add new binding.
        self.list.push_back( (key, add()) ) ;
        // Done
        return false
      }
    }
  }

  /// Backward version of `update_funs`.
  fn update_bak<U, A> (& mut self, key: Key, update: U, add: A) -> bool
  where U: Fn(& mut Val), A: Fn() -> Val {
    let mut new = LinkedList::new() ;
    loop {
      if let Some( (k, mut v) ) = self.list.pop_back() {
        if k > key { new.push_front( (k,v) ) }
        else {
          if k == key {
            // Update binding.
            update(& mut v) ;
            self.list.push_back( (key, v) ) ;
            // Append tail.
            self.list.append( & mut new ) ;
            // Done.
            return true
          } else {
            // Add current binding.
            self.list.push_back( (k,v) ) ;
            // Add new binding.
            self.list.push_back( (key, add()) ) ;
            // Append tail.
            self.list.append( & mut new ) ;
            // Done.
            return false
          }
        }
      } else {
        // Add new binding.
        self.list.push_back( (key, add()) ) ;
        // Append tail.
        self.list.append( & mut new ) ;
        // Done
        return false
      }
    }
  }
}

/// Functions for weight maps.
impl<T: Ord + Copy> Map<Weight, Set<T>> {

  /// Returns true iff the max weight is above the normalization threshold.
  #[inline]
  pub fn should_normalize(& self) -> bool {
    match self.list.back() {
      Some( pair ) => {
        let (ref w, _) = * pair ; w.should_normalize()
      },
      _ => false
    }
  }

  /// Pops the first element of the highest weighted set.
  #[inline]
  pub fn pop_heaviest(& mut self) -> Option<T> {
    loop {
      match self.list.back_mut() {
        Some( pair ) => {
          let (_, ref mut set) = * pair ;
          match set.pop() {
            None => (),
            some => return some,
          }
        },
        _ => return None,
      }
      self.list.pop_back() ;
    }
  }

  /// Calls `normalize` on all the weights and merges the sets of the now equal
  /// weights. (Goes through the map once, forward.)
  pub fn force_normalize(& mut self) {
    if let Some( (mut weight, set) ) = self.list.pop_front() {
      weight.normalize() ;
      let mut prev = weight ;
      let mut old = LinkedList::new() ;
      old.append(& mut self.list) ;
      self.list.push_back( (prev, set) ) ;

      loop {
        if let Some( (mut weight, mut set) ) = old.pop_front() {
          weight.normalize() ;
          if weight == prev {
            // Safe, we're sure there's at least one element there.
            let pair = self.list.back_mut().unwrap() ;
            let (_, ref mut prev_set) = * pair ;
            prev_set.union(& mut set)
          } else {
            assertion!( weight > prev ) ;
            prev = weight ;
            self.list.push_back( (weight, set) ) ;
          }
        } else { break }
      }
    }
  }

  /// Normalizes the weight map and returns true iff `should_normalize` is
  /// true.
  #[inline]
  pub fn normalize(& mut self) -> bool {
    if self.should_normalize() {
      self.force_normalize() ;
      true
    } else { false }
  }

  /// Removes the mappings in `rhs` from `self` and adds the mappings
  /// obtained by incrementing all weights in `rhs` by one.
  pub fn increment(& mut self, rhs: & mut Self) {
    let min =
      if let Some( pair ) = rhs.list.front() { pair.0 }
      else { return () } ;

    if min >= self.pivot {
      // println!("increment bak") ;
      self.increment_bak(rhs)
    } else {
      // println!("increment for") ;
      self.increment_for(rhs)
    }
  }

  fn increment_for(& mut self, rhs: & mut Self) {
    let rhs = & mut rhs.list ;
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    'top: loop {
      // Pop a pair to add.
      if let Some( (mut weight, mut set) ) = rhs.pop_front() {

        loop {

          if let Some( (w, mut s) ) = old.pop_front() {
            if w < weight {
              // Need to pop some more.
              self.list.push_back( (w,s) )
            } else {
              if w == weight {
                // Removing elements to remove.
                s.minus(& set) ;
                // Updating lhs.
                self.list.push_back( (w,s) ) ;
                // Incrementing weight.
                weight.inc() ;
                // Checking next element in lhs.
                if let Some(pair) = old.front() {
                  if pair.0 > weight {
                    // Updating lhs with increased weight.
                    self.list.push_back( (weight, set) ) ;
                    break
                  }
                } else {
                  // No more elements in lhs, updating with increased weight.
                  self.list.push_back( (weight, set) ) ;
                  break
                } ;
                // Adding elements.
                let (w, mut s) = old.pop_front().unwrap() ;
                s.union(& mut set) ;
                // Putting back in old in case next weight is this one.
                old.push_front( (w,s) ) ;
                // Moving on to next weight to increment.
                break
              } else {
                // Weight to increment is not defined, should not happen.
                panic!(
                  "[increment_for] weight to increment is not defined in lhs"
                )
              }
            }
          } else {
            // No more elements in the lhs list, should not happen.
            panic!(
              "[increment_for] weight to increment exceeds lhs max weight"
            )
          }

        }

      } else {
        // No more elements to add.
        self.list.append(& mut old) ;
        break
      }
    }
  }

  fn increment_bak(& mut self, rhs: & mut Self) {
    let rhs = & mut rhs.list ;
    let mut new = LinkedList::new() ;
    'top: loop {
      // Pop a pair to add.
      if let Some( (mut weight, mut set) ) = rhs.pop_back() {

        loop {

          if let Some( (w, mut s) ) = self.list.pop_back() {
            if w > weight {
              // Need to pop some more.
              new.push_front( (w,s) )
            } else {
              if w == weight {
                // Removing elements to remove.
                s.minus(& set) ;
                // Updating lhs.
                new.push_front( (w,s) ) ;
                // Incrementing weight.
                weight.inc() ;
                // Checking next element in lhs.
                if let Some(pair) = self.list.back() {
                  if pair.0 < weight {
                    // Updating lhs with increased weight.
                    new.push_front( (weight, set) ) ;
                    break
                  }
                } else {
                  // No more elements in lhs, updating with increased weight.
                  new.push_front( (weight, set) ) ;
                  break
                } ;
                // Adding elements.
                let (w, mut s) = self.list.pop_back().unwrap() ;
                s.union(& mut set) ;
                // Putting back in list in case next weight is this one.
                self.list.push_back( (w,s) ) ;
                // Moving on to next weight to increment.
                break
              } else {
                // Weight to increment is not defined, should not happen.
                panic!(
                  "[increment_bak] weight to increment is not defined in lhs"
                )
              }
            }
          } else {
            // No more elements in the lhs list, should not happen.
            panic!("
              [increment_bak] weight to increment exceeds lhs max weight"
            )
          }

        }

      } else {
        // No more elements to add.
        self.list.append(& mut new) ;
        break
      }
    }
  }

  /// Merges two weight maps.
  pub fn merge(& mut self, rhs: & mut Self) {
    let min =
      if let Some( pair ) = rhs.list.front() { pair.0 }
      else { return () } ;

    if min >= self.pivot {
      // println!("merge bak") ;
      self.merge_bak(rhs)
    } else {
      // println!("merge for") ;
      self.merge_for(rhs)
    }
  }

  fn merge_for(& mut self, rhs: & mut Self) {
    let mut rhs = & mut rhs.list ;
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    'top: loop {
      // Pop a pair to add.
      if let Some( (weight, mut set) ) = rhs.pop_front() {

        loop {

          if let Some( (w, mut s) ) = old.pop_front() {
            if w < weight {
              // Need to pop some more.
              self.list.push_back( (w,s) )
            } else {
              if w == weight {
                // Adding elements.
                s.union(& mut set) ;
                // Updating lhs.
                self.list.push_back( (w,s) ) ;
                // Moving on to the next weight to merge.
                break
              } else {
                // Adding the new binding.
                self.list.push_back( (weight, set) ) ;
                old.push_front( (w,s) ) ;
                // Moving on to next weight to merge.
                break
              }
            }
          } else {
            self.list.push_back( (weight, set) ) ;
            // No more elements in the lhs list.
            self.list.append(& mut rhs) ;
            // Moving on to next weight to merge.
            break
          }
        }

      } else {
        // No more elements to add.
        self.list.append(& mut old) ;
        break
      }
    }
  }

  fn merge_bak(& mut self, rhs: & mut Self) {
    let mut rhs = & mut rhs.list ;
    let mut new = LinkedList::new() ;
    'top: loop {
      // Pop a pair to add.
      if let Some( (weight, mut set) ) = rhs.pop_back() {

        loop {

          if let Some( (w, mut s) ) = self.list.pop_back() {
            if w > weight {
              // Need to pop some more.
              new.push_front( (w,s) )
            } else {
              if w == weight {
                // Adding elements.
                s.union(& mut set) ;
                // Updating lhs.
                new.push_front( (w,s) ) ;
                // Moving on to next weight to merge.
                break
              } else {
                // Adding the new binding.
                new.push_front( (weight, set) ) ;
                self.list.push_back( (w,s) ) ;
                // Moving on to next weight to merge.
                break
              }
            }
          } else {
            // No more elements in the lhs list.
            self.list.append(& mut rhs) ;
            self.list.push_back( (weight, set) ) ;
            self.list.append(& mut new) ;
            // Moving on to next weight to merge.
            break
          }

        }

      } else {
        // No more elements to add.
        self.list.append(& mut new) ;
        break
      }
    }
  }

  /// Removes the mappings in `rhs` from `self`.
  pub fn minus(& mut self, rhs: & mut Self) {
    let min =
      if let Some( pair ) = rhs.list.front() { pair.0 }
      else { return () } ;

    if min >= self.pivot {
      // println!("minus bak") ;
      self.minus_bak(rhs)
    } else {
      // println!("minus for") ;
      self.minus_for(rhs)
    }
  }

  fn minus_for(& mut self, rhs: & mut Self) {
    let mut rhs = & mut rhs.list ;
    let mut old = LinkedList::new() ;
    old.append(& mut self.list) ;
    'top: loop {
      // Pop a pair to remove.
      if let Some( (weight, mut set) ) = rhs.pop_front() {

        loop {

          if let Some( (w, mut s) ) = old.pop_front() {
            if w < weight {
              // Need to pop some more.
              self.list.push_back( (w,s) )
            } else {
              if w == weight {
                // Removing elements.
                s.minus(& mut set) ;
                // Updating lhs if set is not empty.
                if ! s.is_empty() {
                  self.list.push_back( (w,s) )
                } ;
                // Moving on to the next weight to remove.
                break
              } else {
                // Nothing to do.
                old.push_front( (w,s) ) ;
                // Moving on to next weight to remove.
                break
              }
            }
          } else {
            // Moving on to next weight to remove.
            break
          }
        }

      } else {
        // No more elements to remove.
        self.list.append(& mut old) ;
        break
      }
    }
  }

  fn minus_bak(& mut self, rhs: & mut Self) {
    let mut rhs = & mut rhs.list ;
    let mut new = LinkedList::new() ;
    'top: loop {
      // Pop a pair to remove.
      if let Some( (weight, mut set) ) = rhs.pop_back() {

        loop {

          if let Some( (w, mut s) ) = self.list.pop_back() {
            if w > weight {
              // Need to pop some more.
              new.push_front( (w,s) )
            } else {
              if w == weight {
                // Removing elements.
                s.minus(& mut set) ;
                // Updating lhs if set is not empty.
                if ! s.is_empty() {
                  new.push_front( (w,s) )
                } ;
                // Moving on to next weight to remove.
                break
              } else {
                self.list.push_back( (w,s) ) ;
                // Moving on to next weight to remove.
                break
              }
            }
          } else {
            // No more elements in the lhs list.
            rhs.clear() ;
            self.list.append(& mut new) ;
            // Moving on to next weight to remove.
            break
          }

        }

      } else {
        // No more elements to remove.
        self.list.append(& mut new) ;
        break
      }
    }
  }
}


impl Set<Var> {
  /// Creates the set of all variables up to `last_var`.
  /// The pivot is the `last_var / 2`.
  pub fn new_var_set(last_var: Var) -> Self {
    let mut list = LinkedList::new() ;
    let pivot = last_var / 2 ;
    for v in 1..(last_var + 1) {
      list.push_back(v)
    } ;
    Set { list: list, pivot: pivot }
  }
}

/// Functions for weight maps.
impl Map<Weight, Set<Var>> {
  /// Creates a new weight to variable set map.
  /// The default weight is mapped to the set of all variables.
  /// Pivot is the `Weight::pivot()`.
  pub fn new_weight_var_map(last_var: Var) -> Self {
    let mut list = LinkedList::new() ;
    list.push_back( (Weight::new(), Set::new_var_set(last_var)) ) ;
    Map {
      list: list, pivot: Weight::pivot(),
    }
  }
}



impl<
  Key: Ord + Sized + fmt::Display, Val: Ord + Copy + fmt::Display
> Map<Key, Set<Val>> {
  /// Adds a new value to the set associated with a key in a map if it's there,
  /// creates a new set (with pivot `pivot`) with the value and adds the
  /// binding otherwise.
  pub fn update(& mut self, pivot: Val, key: Key, val: Val) -> bool {
    self.update_funs(key,
      |set: & mut Set<Val>| { set.add(val) },
      || { let mut nu = Set::new(pivot) ; nu.add(val) ; nu }
    )
  }

  /// Adds the elements of `set` to the set associated with a key in a map if
  /// it's there, adds a new binding otherwise. **Copies** `set`.
  pub fn add_set(
    & mut self, key: Key, set: & Set<Val>
  ) -> bool {
    self.update_funs(key,
      |s: & mut Set<Val>| {
        for val in set.iter() {
          s.add(*val)
        }
      },
      || { set.clone() }
    )
  }
}

impl<
  Key: Ord + Copy + fmt::Display, Val: Copy
> Clone for Map<Key, Val> {
  fn clone(& self) -> Self {
    let mut list = LinkedList::new() ;
    for pair in self.iter() {
      list.push_back(* pair) ;
    } ;
    Map { list: list, pivot: self.pivot }
  }
}


impl<
  Key: Ord + Sized + fmt::Display, Val: Sized + fmt::Display
> fmt::Display for Map<Key, Val> {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    try!( write!(fmt, "{{") ) ;
    let mut is_first = true ;
    for pair in self.iter() {
      let (ref key, ref value) = * pair ;
      if is_first { is_first = false }
      else { try!( write!(fmt, ", ") ) }
      try!( write!(fmt, "{}:{}", key, value) ) ;
    } ;
    write!(fmt, "}}")
  }
}

impl<
  Key: Ord + Sized + fmt::Display, Val: Sized + fmt::Display
> fmt::Debug for Map<Key, Val> {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    try!( write!(fmt, "{{") ) ;
    let mut is_first = true ;
    for pair in self.iter() {
      let (ref key, ref value) = * pair ;
      if is_first { is_first = false }
      else { try!( write!(fmt, ", ") ) }
      try!( write!(fmt, "{}:{}", key, value) ) ;
    } ;
    write!(fmt, "}}")
  }
}




// pub fn test() {
//   let ref mut map = Map::<usize,Set<usize>>::new(10) ;
//   println!("> {}", map) ;
//   fn update_print(
//     map: & mut Map<usize, Set<usize>>,key: usize, val: usize
//   ) {
//     let res = map.update(0, key, val) ;
//     println!("Update {}, {}", key, val) ;
//     println!("> [{}] {}", res, map)
//   }
//   fn get_print(map: & Map<usize, Set<usize>>, key: usize) {
//     println!("Get {}", key) ;
//     match map.get(key) {
//       None => println!("> none"),
//       Some(v) => println!("> {}", v),
//     }
//   }

//   update_print(map, 5, 3) ;
//   update_print(map, 5, 4) ;
//   update_print(map, 1, 2) ;
//   update_print(map, 7, 11) ;
//   update_print(map, 6, 3) ;
//   update_print(map, 1, 3) ;
//   update_print(map, 7, 13) ;
//   update_print(map, 7, 11) ;
//   update_print(map, 5, 4) ;
//   update_print(map, 5, 7) ;

//   get_print(map, 6) ;
//   get_print(map, 11) ;
//   get_print(map, 7) ;
//   get_print(map, 1) ;
//   get_print(map, 5) ;
//   get_print(map, 17) ;
// }




// #[cfg(test)]
// fn is_equal_map(
//   map1: & Map<usize,Set<usize>>,
//   map2: & Map<usize,Set<usize>>
// ) -> bool {
//   let len = map1.len() ;
//   if len != map2.len() { false }
//   else {
//     let (mut iter1, mut iter2) = (map1.iter(), map2.iter()) ;
//     for _ in 0..len {
//       match (iter1.next(), iter2.next()) {
//         (Some(p1), Some(p2)) => {
//           let (ref k1, ref s1) = *p1 ;
//           let (ref k2, ref s2) = *p2 ;
//           if k1 != k2 { return false } ;
//           if ! is_equal(s1, s2) { return false } ;
//         },
//         // Unreachable, size was checked before.
//         _ => panic!("unreachable"),
//       }
//     } ;
//     true
//   }
// }


#[test]
fn sorted_set_add_rm() {
  use rand::{Rand, OsRng} ;

  fn add(
    set: & mut Set<usize>, set_back: & mut Set<usize>, n: usize
  ) {
    set.test_add_for(n) ;
    set_back.test_add_bak(n) ;
    println!("Added {}", n) ;
    println!("[front] {}", set) ;
    println!("[back]  {}", set_back) ;
  }

  fn rm(
    set: & mut Set<usize>, set_back: & mut Set<usize>, n: usize
  ) {
    set.test_rm_for(n) ;
    set_back.test_rm_bak(n) ;
    println!("Removed {}", n) ;
    println!("[front] > {}", set) ;
    println!("[back]  > {}", set_back) ;
  }

  let pivot = (!0) / 2 ;
  let mut set = Set::new(pivot) ;
  let set_ref = & mut set ;
  let mut set_back = Set::new(pivot) ;
  let set_back_ref = & mut set_back ;

  let mut rng = match OsRng::new() {
    Ok(rng) => rng,
    Err(s) => panic!("could not create rng: {}", s),
  } ;
  let test_size = 200 ;

  let vec = {
    let mut vec = Vec::with_capacity(test_size) ;
    for _ in 0..test_size {
      vec.push(usize::rand(& mut rng))
    } ;
    vec
  } ;

  for n in vec.iter() {
    add(set_ref, set_back_ref, * n) ;
    assert!( set_ref.mem(* n) ) ;
    assert!( set_back_ref.mem(* n) ) ;
    assert!( set_ref.is_sorted() ) ;
    assert!( set_back_ref.is_sorted() ) ;
    assert_eq!( set_ref, set_back_ref ) ;
  } ;
  println!("") ;
  for n in vec.iter() {
    rm(set_ref, set_back_ref, * n) ;
    assert!( set_ref.is_sorted() ) ;
    assert!( set_back_ref.is_sorted() ) ;
    assert_eq!( set_ref, set_back_ref ) ;

  } ;
  println!("") ;

}

#[test]
fn sorted_map_update() {
  use rand::{Rand, OsRng} ;

  fn update(
    map: & mut Map<usize, Set<usize>>, key: usize, val: usize,
    pivot: usize
  ) {
    map.update(pivot, key, val) ;
    println!("Added {} -> {}", key, val) ;
    println!("[front] {}", map) ;
  }

  let pivot = (!0) / 2 ;
  let mut map = Map::<usize, Set<usize>>::new(pivot) ;
  let map_ref = & mut map ;

  let mut rng = match OsRng::new() {
    Ok(rng) => rng,
    Err(s) => panic!("could not create rng: {}", s),
  } ;
  let test_size = 300 ;

  let vec = {
    let mut vec = Vec::with_capacity(test_size) ;
    for _ in 0..test_size {
      vec.push(
        (usize::rand(& mut rng), usize::rand(& mut rng))
      )
    } ;
    vec
  } ;

  for (key, val) in vec.into_iter() {
    let mut res_set = match map_ref.get(key) {
      None => Set::new(pivot),
      Some(s) => s.clone(),
    } ;
    res_set.add(val) ;
    update(map_ref, key, val, pivot) ;
    match map_ref.get(key) {
      None => panic!("key {} is not defined in map", key),
      Some(set) => assert_eq!( set, & res_set ),
    }
  } ;
  println!("") ;

}


#[test]
pub fn set_union_minus() {

  let v1 = vec![4,7,3,5,2,9] ;
  let empty = vec![] ;

  let v2 = vec![4,6,7] ;
  let u_1_2 = vec![2,3,4,5,6,7,9] ;
  let m_1_2 = vec![2,3,5,9] ;

  let v3 = vec![3,4,7,10,11] ;
  let u_1_3 = vec![2,3,4,5,7,9,10,11] ;
  let m_1_3 = vec![2,5,9] ;

  let v4 = vec![1,2,3] ;
  let u_1_4 = vec![1,2,3,4,5,7,9] ;
  let m_1_4 = vec![4,5,7,9] ;

  let v5 = vec![1,2,4,6,7,10,11] ;
  let u_1_5 = vec![1,2,3,4,5,6,7,9,10,11] ;
  let m_1_5 = vec![3,5,9] ;

  let test_cases = vec![
    ( 5, &empty, &empty, &empty, &empty, "empty rhs/lhs"),
    ( 5, &v1, &empty, &v1, &v1, "empty rhs"),
    ( 5, &empty, &v1, &v1, &empty, "empty lhs"),
    ( 10, &v1, &v2, &u_1_2, &m_1_2, "rhs in lhs (for)"),
    ( 0, &v1, &v2, &u_1_2, &m_1_2, "rhs in lhs (bak)"),
    ( 10, &v1, &v3, &u_1_3, &m_1_3, "rhs above lhs (for)"),
    ( 0, &v1, &v3, &u_1_3, &m_1_3, "rhs above lhs (bak)"),
    ( 10, &v1, &v4, &u_1_4, &m_1_4, "rhs below lhs (for)"),
    ( 0, &v1, &v4, &u_1_4, &m_1_4, "rhs below lhs (bak)"),
    ( 10, &v1, &v5, &u_1_5, &m_1_5, "lhs in rhs (for)"),
    ( 0, &v1, &v5, &u_1_5, &m_1_5, "lhs in rhs (bak)"),
  ] ;

  fn test_union(
    pivot: usize,
    v1: & Vec<usize>,
    v2: & Vec<usize>,
    res: & Vec<usize>,
    bla: & str
  ) {
    let mut s1 = {
      let mut s = Set::new(pivot) ;
      for v in v1.iter() { s.add(* v) } ;
      s
    } ;
    let mut s2 = {
      let mut s = Set::new(pivot) ;
      for v in v2.iter() { s.add(* v) } ;
      s
    } ;
    let res = {
      let mut s = Set::new(pivot) ;
      for v in res.iter() { s.add(* v) } ;
      s
    } ;

    println!("") ;
    println!("union: {}", bla) ;
    s1.union(& mut s2) ;
    if ! s1.is_sorted() {
      println!("s1: {}", s1) ;
      panic!("[{}, union]: s1 is not sorted", bla)
    } ;
    if s1 != res {
      println!("s1: {}", s1) ;
      println!("res: {}", res) ;
      panic!("[{}, union]: s1 != res", bla)
    } ;
  }

  fn test_minus(
    pivot: usize,
    v1: & Vec<usize>,
    v2: & Vec<usize>,
    res: & Vec<usize>,
    bla: & str
  ) {
    let mut s1 = {
      let mut s = Set::new(pivot) ;
      for v in v1.iter() { s.add(* v) } ;
      s
    } ;
    let s2 = {
      let mut s = Set::new(pivot) ;
      for v in v2.iter() { s.add(* v) } ;
      s
    } ;
    let res = {
      let mut s = Set::new(pivot) ;
      for v in res.iter() { s.add(* v) } ;
      s
    } ;

    println!("") ;
    println!("minus: {}", bla) ;
    s1.minus(& s2) ;
    if ! s1.is_sorted() {
      panic!("[{}, minus]: result is not sorted", bla)
    } ;
    if s1 != res {
      panic!("[{}, minus]: s1 != res", bla)
    } ;
  }

  for (p, v1, v2, union, minus, bla) in test_cases.into_iter() {
    test_union(p, v1, v2, union, bla) ;
    test_minus(p, v1, v2, minus, bla) ;
  }
}


// #[cfg(any(bench))]
// use test::Bencher ;
// #[cfg(any(bench, test))]
// use rand::{Rand, SeedableRng, StdRng} ;

// #[cfg(any(bench, test))]
// static bench_size: usize = 2_000 ;

// #[bench]
// fn sorted_list_add_forward(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng: StdRng = SeedableRng::from_seed(seed) ;
//   bench.iter(|| {
//     let mut set = Set::new(0) ;
//     for _ in 0..bench_size {
//       set.test_add_for( usize::rand(& mut rng) )
//     } ;
//     assert!(set.is_sorted())
//   } )
// }
// #[bench]
// fn sorted_list_rm_forward(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng_add: StdRng = SeedableRng::from_seed(seed) ;
//   let mut rng_rm: StdRng = SeedableRng::from_seed(seed) ;
//   let pivot = (!0) / 2 ;
//   bench.iter(|| {
//     let mut set = Set::new(pivot) ;
//     for _ in 0..bench_size {
//       set.add( usize::rand(& mut rng_add) )
//     } ;
//     assert!(set.is_sorted()) ;
//     for _ in 0..bench_size {
//       set.test_rm_for( usize::rand(& mut rng_rm) )
//     }
//   } )
// }

// #[bench]
// fn sorted_list_add_backward(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng: StdRng = SeedableRng::from_seed(seed) ;
//   bench.iter(|| {
//     let mut set = Set::new(0) ;
//     for _ in 0..bench_size {
//       set.test_add_bak( usize::rand(& mut rng) )
//     } ;
//     assert!(set.is_sorted())
//   } )
// }
// #[bench]
// fn sorted_list_rm_backward(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng_add: StdRng = SeedableRng::from_seed(seed) ;
//   let mut rng_rm: StdRng = SeedableRng::from_seed(seed) ;
//   let pivot = (!0) / 2 ;
//   bench.iter(|| {
//     let mut set = Set::new(pivot) ;
//     for _ in 0..bench_size {
//       set.add( usize::rand(& mut rng_add) )
//     } ;
//     assert!(set.is_sorted()) ;
//     for _ in 0..bench_size {
//       set.test_rm_bak( usize::rand(& mut rng_rm) )
//     }
//   } )
// }

// #[bench]
// fn sorted_list_add(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng: StdRng = SeedableRng::from_seed(seed) ;
//   let pivot = (!0) / 2 ;
//   bench.iter(|| {
//     let mut set = Set::new(pivot) ;
//     for _ in 0..bench_size {
//       set.add( usize::rand(& mut rng) )
//     } ;
//     assert!(set.is_sorted())
//   } )
// }
// #[bench]
// fn sorted_list_rm(bench: & mut Bencher) {
//   let seed: &[_] = &[7, 3, 11, 17];
//   let mut rng_add: StdRng = SeedableRng::from_seed(seed) ;
//   let mut rng_rm: StdRng = SeedableRng::from_seed(seed) ;
//   let pivot = (!0) / 2 ;
//   bench.iter(|| {
//     let mut set = Set::new(pivot) ;
//     for _ in 0..bench_size {
//       set.add( usize::rand(& mut rng_add) )
//     } ;
//     assert!(set.is_sorted()) ;
//     for _ in 0..bench_size {
//       assert!( set.rm( usize::rand(& mut rng_rm) ) )
//     }
//   } )
// }
