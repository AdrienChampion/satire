#![ doc = "
"]

use time::{Duration, SteadyTime} ;

use ::conf::Conf ;
use ::common::Statistics ;

/// Maintains timers and counters for the statistics of an instance.
pub struct Stats {
  decisions: usize,
  assignments: usize,
  conflicts: usize,
  restarts: usize,
  resolution_time: Duration,
  resolution_timer: SteadyTime,
  backjump_time: Duration,
  backjump_timer: SteadyTime,
  decision_time: Duration,
  decision_timer: SteadyTime,
  propagation_time: Duration,
  propagation_timer: SteadyTime,
  total_time: Duration,
  total_timer: SteadyTime,
  timeout: Option<Duration>,
}

impl Stats {
  /// Creates a new statistics structure.
  pub fn new(conf: & Conf) -> Stats {
    let timeout = if conf.timeout() == 0 {
      None
    } else {
      Some(Duration::seconds(
        conf.timeout() as i64
      ))
    } ;
    Stats {
      decisions: 0, assignments: 0, conflicts: 0, restarts: 0,
      resolution_time: Duration::zero(),
      resolution_timer: SteadyTime::now(),
      backjump_time: Duration::zero(),
      backjump_timer: SteadyTime::now(),
      decision_time: Duration::zero(),
      decision_timer: SteadyTime::now(),
      propagation_time: Duration::zero(),
      propagation_timer: SteadyTime::now(),
      total_time: Duration::zero(),
      total_timer: SteadyTime::now(),
      timeout: timeout,
    }
  }

  /// Increments the number of decisions by one.
  #[inline]
  pub fn decisions_inc(& mut self) { self.decisions += 1 }
  /// Increments the number of assignments by one.
  #[inline]
  pub fn assignments_inc(& mut self) { self.assignments += 1 }
  /// Increments the number of conflicts by one.
  #[inline]
  pub fn conflicts_inc(& mut self) { self.conflicts += 1 }
  /// Increments the number of restarts by one.
  #[inline]
  pub fn restarts_inc(& mut self) { self.restarts += 1 }

  /// Starts the resolution timer.
  #[inline]
  pub fn resolution_start(& mut self) {
    self.resolution_timer = SteadyTime::now()
  }
  /// Stops the resolution timer and adds the time since the last start to
  /// the resolution time.
  #[inline]
  pub fn resolution_stop(& mut self) {
    self.resolution_time =
      self.resolution_time + (SteadyTime::now() - self.resolution_timer)
  }

  /// Starts the backjump timer.
  #[inline]
  pub fn backjump_start(& mut self) {
    self.backjump_timer = SteadyTime::now()
  }
  /// Stops the backjump timer and adds the time since the last start to
  /// the backjump time.
  #[inline]
  pub fn backjump_stop(& mut self) {
    self.backjump_time =
      self.backjump_time + (SteadyTime::now() - self.backjump_timer)
  }

  /// Starts the decision timer.
  #[inline]
  pub fn decision_start(& mut self) {
    self.decision_timer = SteadyTime::now()
  }
  /// Stops the decision timer and adds the time since the last start to
  /// the decision time.
  #[inline]
  pub fn decision_stop(& mut self) {
    self.decision_time =
      self.decision_time + (SteadyTime::now() - self.decision_timer)
  }
  /// Starts the propagation timer.
  #[inline]
  pub fn propagation_start(& mut self) {
    self.propagation_timer = SteadyTime::now()
  }
  /// Stops the propagation timer and adds the time since the last start to
  /// the propagation time.
  #[inline]
  pub fn propagation_stop(& mut self) {
    self.propagation_time =
      self.propagation_time + (SteadyTime::now() - self.propagation_timer)
  }
  /// Starts the total timer.
  #[inline]
  pub fn total_start(& mut self) {
    self.total_timer = SteadyTime::now()
  }
  /// Stops the total timer and adds the time since the last start to
  /// the total time.
  #[inline]
  pub fn total_stop(& mut self) {
    self.total_time =
      self.total_time + (SteadyTime::now() - self.total_timer)
  }
}

impl Statistics for Stats {
  fn decisions(& self) -> usize { self.decisions }
  fn assignments(& self) -> usize { self.assignments }
  fn conflicts(& self) -> usize { self.conflicts }
  fn restarts(& self) -> usize { self.restarts }
  fn resolution_time(& self) -> Duration {
    self.resolution_time
  }
  fn backjump_time(& self) -> Duration {
    self.backjump_time
  }
  fn decision_time(& self) -> Duration {
    self.decision_time
  }
  fn propagation_time(& self) -> Duration {
    self.propagation_time
  }
  /// **Returns zero** unless the total timer has been stop. Use `current_time`
  /// while the analysis is running.
  fn total_time(& self) -> Duration {
    self.total_time
  }
  #[inline]
  fn current_time(& self) -> Duration {
    SteadyTime::now() - self.total_timer
  }
  #[inline]
  fn timeout_reached(& self) -> bool {
    if let Some(timeout) = self.timeout {
      self.current_time() >= timeout
    } else { false }
  }

}