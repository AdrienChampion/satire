#![ doc = "
A clause is a vector of literals following the watched literals scheme. It also
contains its index in the CNF.

# Assumptions

* a clause mentions at least two literals (otherwise it's a valuation),
* no literal is mentioned twice.

"]

use std::fmt ;
use std::slice::Iter ;

use ::common::{
  Lit, Size, Index, CIndex, CStatus, VarMap
} ;



// The following macros are shorthands for the functions moving the literal
// watchers.
macro_rules! move_wl_match {
  ($slf:ident, $index:ident, $var_map:ident, $one:ident, $two:ident) => (
    match $var_map.val_of_lit($slf.lit_at($index)) {
      // Undef found.
      None => {
        // Skipping if the other `two` points at it.
        if $index != $slf.$two {
          $slf.$one = $index ;
          return None
        }
      },
      // Nothing to do here.
      Some(false) => (),
      // Clause is sat.
      Some(true) => return Some(true),
    }
  ) ;
}
macro_rules! move_wl {
  ($slf:ident -> $one:ident, $two:ident | $var_map:ident) => (
    for index in ($slf.$one + 1)..($slf.vec.len())
      { move_wl_match!($slf, index, $var_map, $one, $two) }
  ) ;
  ($slf:ident <- $one:ident, $two:ident | $var_map:ident) => (
    for index in 0..($slf.$one)
      { move_wl_match!($slf, index, $var_map, $one, $two) }
  ) ;
}




/// A clause is a vector of literals with a two watched literal mechanism. It
/// also contains its index in the cnf.
///
/// See [module level documentation][moduledoc] for more info.
///
/// [moduledoc]: index.html (Module level documentation.)
pub struct Clause {
  // The vector of literals.
  vec: Vec<Lit>,
  // The first literal watcher.
  wl1: Index,
  // The second literal watcher.
  wl2: Index,
  // The index of this clause.
  index: CIndex,
}

impl Clause {

  /// Creates a new `Clause` from a vector of literals.
  pub fn new(vec: Vec<Lit>, index: CIndex) -> Clause {
    // if vec.len() < 2 {
    //   panic!("cannot create a clause with less than two literals")
    // } ;
    Clause {
      vec: vec, wl1: (0 as Size), wl2: (1 as Size), index: index,
    }
  }

  /// Constructs a clause from a string.
  pub fn of_str( s: & str, index: CIndex ) -> Result<Clause,Lit> {
    let mut res = Vec::new() ;
    for lit in s.trim().split(" ") {
      if ! lit.is_empty() {
        match lit.parse::<isize>() {
          Ok(0) => (),
          Ok(val) => res.push( Lit{ l: val } ),
          Err(_) => panic!(
            "[parsing] i32 expected but found \"{}\"", lit
          ),
        }
      }
    } ;
    match res.len() {
      0 => panic!(
        "[parsing] no literal, clause creation failed"
      ),
      1 => Err(res[0]),
      _ => Ok( Clause::new( res, index ) ),
    }
  }

  /// The number of literals.
  pub fn len(& self) -> Size { self.vec.len() }

  /// The index of this clause in the cnf.
  #[inline(always)]
  pub fn index(& self) -> CIndex { self.index }

  /// An iterator on its internal literals.
  #[inline(always)]
  pub fn iter<'a>(& 'a self) -> Iter<'a,Lit> { self.vec.iter() }

  /// The literal at `i` in a clause.
  #[inline(always)]
  pub fn lit_at(& self, i: Index) -> Lit { self.vec[i] }

  /// The first watched literal.
  #[inline(always)]
  fn wl1(& self) -> Lit { self.lit_at(self.wl1) }
  /// The second watched literal.
  #[inline(always)]
  fn wl2(& self) -> Lit { self.lit_at(self.wl2) }
  #[cfg(test)]
  /// The index of the first watched literal.
  pub fn wl1_index(& self) -> Index { self.wl1 }
  #[cfg(test)]
  /// The index of the second watched literal.
  pub fn wl2_index(& self) -> Index { self.wl2 }

  /// Moves `wl1` and returns its new value. Tries to go forward first, and
  /// then backwards.
  #[inline]
  fn move_wl1(& mut self, var_map: & VarMap) -> Option<bool> {
    // Can we move forward?
    move_wl!( self -> wl1, wl2 | var_map) ;
    // Can we move backward?
    move_wl!( self <- wl1, wl2 | var_map) ;
    // We could not move, so we're false.
    Some(false)
  }

  /// Moves `wl2` and returns its new value. Tries to go backwards first, and
  /// then forward.
  #[inline]
  fn move_wl2(& mut self, var_map: & VarMap) -> Option<bool> {
    // Can we move forward?
    move_wl!( self -> wl2, wl1 | var_map) ;
    // Can we move backward?
    move_wl!( self <- wl2, wl1 | var_map) ;
    // We could not move, so we're false.
    Some(false)
  }

  /// Updates a clause based on an assignment. Returns the new status of the
  /// clause.
  pub fn update(
    & mut self, var_map: & VarMap
  ) -> CStatus {
    let wl1_lit = self.wl1() ;
    let wl1 = var_map.val_of_lit(wl1_lit) ;
    match wl1 {
      // Clause is sat.
      Some(true) => CStatus::Sat,
      // Let's look at w2 in case we can return early.
      _ => {
        let wl2_lit = self.wl2() ;
        let wl2 = var_map.val_of_lit(wl2_lit) ;
        match (wl1, wl2) {
          // Need to move wl1.
          (Some(false), None) => match self.move_wl1(var_map) {
            None => CStatus::Undef,
            Some(false) => CStatus::Unit(wl2_lit),
            Some(true) => CStatus::Sat,
          },
          // Need to move wl2.
          (None, Some(false)) => match self.move_wl2(var_map) {
            None => CStatus::Undef,
            Some(false) => CStatus::Unit(wl1_lit),
            Some(true) => CStatus::Sat,
          },
          // Clause is sat.
          (_, Some(true)) => CStatus::Sat,
          // Need to move someone.
          (Some(false), Some(false)) => match self.move_wl1(var_map) {
            None => CStatus::Undef,
            Some(false) => CStatus::Unsat,
            Some(true) => CStatus::Sat,
          },
          // Clause is undef.
          (None, None) => CStatus::Undef,
          // Not reachable if the assumptions of `Clause` are respected.
          _ => panic!(
            "unexpected values for watched literals in clause {}", self
          )
        }
      },
    }
  }

}



// |===| Display things.


impl fmt::Display for Clause {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    try!(write!(fmt, "{{ ")) ;
    for count in 0..(self.vec.len()) {
      let pref = if count == 0 { "" } else { ", " } ;
      if self.wl1 == count {
        try!(write!(fmt, "{}({})", pref, self.lit_at(count)))
      } else {
        if self.wl2 == count {
          try!(write!(fmt, "{}[{}]", pref, self.lit_at(count)))
        } else {
          try!(write!(fmt, "{}{} ", pref, self.lit_at(count)))
        }
      }
    } ;
    write!(fmt, " }}")
  }
}



// |===| Test things.


// #[test]
// pub fn clause_creation() {
//   let vec: Vec<Lit> = vec![ 1, -2, -3 ].iter().map(
//     |l| Lit { l: *l }
//   ).collect() ;
//   let size = vec.len() ;
//   let clause = Clause::new(vec,1) ;
//   assert!(clause.len() == size) ;
//   assert!(clause.wl1_index() == 0) ;
//   assert!(clause.wl2_index() == 1) ;
// }


// #[test]
// pub fn watched_lit_update() {
//   use ::assignment::ChronoAssignment ;
//   use ::types::Valuation ;

//   let vec: Vec<Lit> = vec![ 1, -2, -3, 4, -5 ].iter().map(
//     |l| Lit { l: *l }
//   ).collect() ;
//   let size = vec.len() ;
//   let mut clause = Clause::new(vec,0) ;

//   println!("initial clause: {}", clause) ;

//   let mut assignment = ChronoAssignment::new(size, vec![]) ;

//   let test_case = vec![
//     (Valuation::new(2, true), CStatus::Undef),
//     (Valuation::new(5, true), CStatus::Undef),
//     (Valuation::new(1, false), CStatus::Undef),
//     (Valuation::new(4, false), CStatus::Unit(Lit::of_var(3).neg())),
//     (Valuation::new(3, false), CStatus::Sat),
//   ] ;

//   for (valuation, res) in test_case {
//     println!("") ;
//     println!("clause: {}", clause) ;
//     assignment.add(valuation) ;
//     println!("   ass: ({})", assignment) ;
//     let got = clause.update(& assignment) ;
//     println!("   got: {} (expected {})", got, res) ;
//     println!("clause: {}", clause) ;
//     assert!(res == got)
//   }

//   println!("") ;
//   println!("backjumping ass to 1") ;
//   assignment.backtrack( 1 ) ;
//   println!("  {}", assignment) ;

//   let test_case = vec![
//     (Valuation::new(3, true), CStatus::Undef),
//     (Valuation::new(1, false), CStatus::Unit(Lit::of_var(4))),
//     (Valuation::new(4, false), CStatus::Unsat),
//   ] ;

//   for (valuation, res) in test_case {
//     println!("") ;
//     println!("clause: {}", clause) ;
//     assignment.add(valuation) ;
//     println!("   ass: ({})", assignment) ;
//     let got = clause.update(& assignment) ;
//     println!("   got: {} ({})", got, res) ;
//     println!("clause: {}", clause) ;
//     assert!(res == got)
//   }


// }






