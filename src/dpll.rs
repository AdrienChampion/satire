#![ doc = "
"]

use std::fmt ;

use ::common::* ;
use ::instance::Instance ;



/// Give access to the underlying instance.
pub trait HasInstance {
  /// The instance underlying instance.
  fn instance(& mut self) -> & mut Instance ;
}

/// Provides dpll reasoning from decision, propagation, conflict resolution and
/// backjumping.
pub trait Dpll: HasInstance + fmt::Display {

  /// The current decision level.
  fn lvl(& self) -> Lvl ;

  /// Checks if the current assignment is a model.
  fn is_model(& self) -> bool ;

  /// The current decision variable.
  fn dec_var(& self) -> Var ;

  /// The decision variable at `lvl`.
  fn dec_var_of(& self, lvl: Lvl) -> Var ;

  /// Prints the current valuations.
  fn print_valuations(& self, prefix: & str) ;

  /// Decides of the value of a variable. Returns none if no variable can be
  /// assigned (sat) and some of the variable decided otherwise.
  fn decide(& mut self) -> Option<Var> ;

  /// Returns `Some((var, index))` if propagation resulted on a conflict on
  /// clause `index` when propagating `var`, and `None` otherwise.
  fn propagate(& mut self, lvl: Lvl, var: Var) -> Option<(Var,CIndex)> ;

  /// Attemps to resolve a conflict on `clause` from `var`. Returns `None` if
  /// the conflict cannot be solved.
  fn resolve(
    & mut self, lvl: Lvl, var: Var, clause: CIndex
  ) -> (Lvl, Var) ;

  /// Restarts or backjumps the instance to decision level `lvl`. Returns None
  /// if backjumping failed, and some of the variable to propagate otherwise.
  fn backjump(& mut self, lvl: Lvl, var: Var) -> Option<Var> ;

  /// Prints the state of the solver.
  fn print(& self) ;

  /// Times a call to `decide`.
  #[inline]
  fn timed_decide(& mut self) -> Option<Var> {
    self.instance().stats.decision_start() ;
    let result = self.decide() ;
    self.instance().stats.decision_stop() ;
    result
  }

  /// Times a call to `propagate`.
  #[inline]
  fn timed_propagate(& mut self, lvl: Lvl, var: Var) -> Option<(Var, CIndex)> {
    self.instance().stats.propagation_start() ;
    let result = self.propagate(lvl, var) ;
    self.instance().stats.propagation_stop() ;
    result
  }

  /// Times a call to `resolve`.
  #[inline]
  fn timed_resolve(
    & mut self, lvl: Lvl, var: Var, clause: CIndex
  ) -> (Lvl, Var) {
    self.instance().stats.resolution_start() ;
    let result = self.resolve(lvl, var, clause) ;
    self.instance().stats.resolution_stop() ;
    result
  }

  /// Times a call to `backjump`.
  #[inline]
  fn timed_backjump(& mut self, lvl: Lvl, var: Var) -> Option<Var> {
    self.instance().stats.backjump_start() ;
    let result = self.backjump(lvl, var) ;
    self.instance().stats.backjump_stop() ;
    result
  }

  /// Solves an instance.
  fn solve(
    & mut self, assumptions: Vec<Lit>,
  ) -> SolverRes {
    use ::common::Source::* ;
    use ::common::SolverRes::* ;

    let mut result = None ;

    self.instance().stats.total_start() ;

    // Propagate singleton clauses.
    // A singleton clause is considered a decision at level 0.
    log1!("") ;
    log1!("propagating singleton clauses (level 0)") ;
    let from = Decision(0) ;
    loop {

      assertion!( run self.instance().check_consistency() ) ;

      if let Some(lit) = self.instance().pop_singleton() {
        log1!("| {}, propagating", lit) ;
        let (var, val) = lit.to_val(from) ;
        self.instance().mut_info_of(var).value = Some(val) ;

        match self.timed_propagate(0, var) {
          // No conflict, moving on.
          None => {
            log1!(" \\ no conflict")
          },
          // Conflict on clause `index`, done: unsat.
          Some(_) => {
            log1!(" \\ conflict, unsat") ;
            result = Some(Unsat) ;
            break
          },
        } ;

        if self.instance().stats.timeout_reached() {
          result = Some(Timeout) ;
          break
        }
      } else {
        log1!("| done") ;
        break
      }
    } ;
    log1!("") ;

    if result == None {
      log1!("propagating assumptions (level 1)") ;
      // Propagate assumptions.
      // An assumption is considered a decision at level 1.
      let from = Decision(1) ;
      for lit in assumptions {
        log1!("| {}, propagating", lit) ;
        let (var, val) = lit.to_val(from) ;
        self.instance().mut_info_of(var).value = Some(val) ;
        // TODO: remove from weight map.
        match self.timed_propagate(1, var) {
          // No conflict, moving on.
          None => {
            log1!(" \\ no conflict")
          },
          // Conflict on clause `index`, done: unsat.
          Some(_) => {
            log1!(" \\ conflict, unsat") ;
            result = Some(Unsat) ;
            break
          },
        } ;

        if self.instance().stats.timeout_reached() {
          result = Some(Timeout) ;
          break
        }
      }
      log1!("| done") ;
    } ;
    log1!("") ;

    // Attempting to make the first decision. Decision level starts at 2.
    assertion!( eq self.lvl(), 1 ) ;

    if result == None {
      log1!("first decision (level 2)") ;
      match self.timed_decide() {
        // Took a decision, updating.
        Some(_) => {
          log1!(
            "| {}{}",
            { let v = self.dec_var() ;
              if self.instance()[v].hint {"+"} else {"-"} },
            self.dec_var()
          ) ;
        },
        // No decision is possible, done: sat.
        None => {
          log1!("| failure, sat") ;
          result = Some(Sat)
        },
      }
    } ;

    if result == None {

      let mut to_propagate = self.dec_var() ;

      // Going to propagation and decision / backjump loop.
      loop {
        let level = self.lvl() ;

        if self.instance().stats.timeout_reached() {
          result = Some(Timeout) ;
          break
        } ;

        // Propagation.
        log1!("") ;
        log1!("decision level is {}", level) ;
        log1!("| propagating {}", to_propagate) ;

        match self.timed_propagate(level, to_propagate) {

          // No conflict, decision time.
          None => {
            // self.print() ;
            log1!(" \\ no conflict, deciding") ;
            match self.timed_decide() {
              // Took a decision, updating.
              Some(v) => {
                log1!(
                  "  | {}{}",
                  if self.instance()[v].hint {"+"} else {"-"}, v
                ) ;
                to_propagate = v
              },
              // No decision is possible, done: sat.
              None => {
                log1!("  | failure, sat") ;
                result = Some(Sat) ;
                break
              },
            }
          },

          // Conflict, attempting to backjump.
          Some((conflict_var, conflict_clause)) => {
            log1!(
              " \\ conflict for variable {} on clause {}: {}",
              conflict_var, conflict_clause,
              self.instance()[conflict_clause]
            ) ;
            if self.lvl() < 2 {
              log1!(
                "  | cannot backjump from level {}, unsat", self.lvl()
              ) ;
              result = Some(Unsat) ;
              break
            } ;
            let (lvl, uip) = self.timed_resolve(
              level, conflict_var, conflict_clause
            ) ;
            log1!(
              "  | resolved, uip is now {}{}",
              if self.instance()[uip].hint {"+"} else {"-"},
              uip
            ) ;
            log1!(
              "  | backjumping to decision level {} (current {})",
              lvl, self.lvl()
            ) ;
            match self.timed_backjump(lvl, uip) {
              None => {
                log1!("   \\ failed, unsat") ;
                result = Some(Unsat) ;
                break
              },
              Some( var ) => {
                log1!(
                  "   \\ {}",
                  if self.lvl() != lvl {
                    format!(
                      "cancelled, restarting on {}{}",
                      if self.instance()[var].hint {"+"} else {"-"},
                      var
                    )
                  } else { "success".to_string() }
                ) ;
                to_propagate = var
              },
            }
          },
        }

      }
    } ;
    // logln!("") ;

    self.instance().stats.total_stop() ;

    match result {
      Some(result) => result,
      None => panic!(
        "solver did not reach a conclusion and did not timeout"
      ),
    }
  }
}

