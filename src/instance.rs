#![ doc = "
An instance is the representation of the SAT problem satire runs on.
"]

use std::fs::File ;
use std::io::{BufReader, BufRead} ;
use std::path::Path ;
use std::iter::IntoIterator ;
use std::fmt ;
use std::ops ;
use std::cmp::min ;

use ::conf::Conf ;
use ::stats::Stats ;
use ::common::* ;
use ::clause::Clause ;
use ::weight::* ;

/// A satire SAT instance.
pub struct Instance {
  /// The original cnf of the instance.
  cnf: Cnf,
  /// The clauses learnt during conflict resolution.
  learnt: Cnf,
  /// The map from variables to their information.
  pub var_map: VarMap,
  /// The statistics of the instance.
  pub stats: Stats,
  /// The weight to variable map.
  weight_var_map: WeightVarMap,
  /// Singleton clauses from the original instance.
  singleton_clauses: Vec<Lit>,
}

impl Instance {

  /// Creates a new instance.
  fn new_of_map(
    conf: & Conf,
    cnf: Cnf,
    var_map: VarMap,
    weight_var_map: WeightVarMap,
    singleton_clauses: Vec<Lit>
  ) -> Instance {

    let size = cnf.len() ;
    Instance {
      cnf: cnf,
      learnt: Vec::with_capacity(size),
      var_map: var_map,
      stats: Stats::new(conf),
      weight_var_map: weight_var_map,
      singleton_clauses: singleton_clauses,
    }
  }

  /// Creates a new instance.
  fn new (
    conf: & Conf, cnf: Vec<Clause>,
    var_map: Vec<(Vec<Index>,  Size)>,
    singleton_clauses: Vec<Lit>
  ) -> Instance {
    let mut weight_var_map = WeightVarMap::new(Weight::pivot()) ;
    let mut var = 0 ;
    let var_pivot = var_map.len() / 2 ;
    let var_map = var_map.into_iter().map(
      |(cs,neg_count)| {
        let cs_size = cs.len() ;
        let (hint,weight) = if neg_count <= cs_size / 2 {
          ( true, Weight::of_u8(min(cs_size - neg_count, 150) as u8) )
        } else {
          ( false, Weight::of_u8(min(neg_count, 150) as u8) )
        } ;
        if var != 0 {
          weight_var_map.update(var_pivot, weight, var) ;
        } ;
        var += 1 ;
        VarInfo {
          clauses: cs,
          clauses_learnt: IndexSet::new(cs_size),
          value: None,
          hint: hint,
          weight: weight,
          conflict_kids: 0
        }
      }
    ).collect() ;
    Instance::new_of_map(
      conf, cnf,
      VarMap::new( var_map ),
      weight_var_map,
      singleton_clauses,
    )
  }

  /// Attempts to create an instance from the file `path` points to.
  pub fn of_path(
    path: & Path, conf: & Conf
  ) -> Result<Instance, & 'static str> {
    of_path( conf, path, Instance::new )
  }


  /// Prints the current valuations.
  pub fn print_valuations(& self, prefix: & str) {
    let max_per_line = 10 ;
    let mut count = 0 ;
    for var in 1..(self.var_count() + 1) {
      if count == 0 {
        log!(nlb, "{}", prefix)
      } ;
      let VarInfo { ref value, .. } = self[var] ;
      print!(
        "{:>4}, ",
        format!(
          "{}{}",
          match * value {
            None => "?",
            Some( Valuation { val: true, .. } ) => "+",
            Some( Valuation { val: false, .. } ) => "-",
          },
          var
        )
      ) ;
      count += 1 ;
      if count >= max_per_line {
        println!("") ;
        count = 0
      }
    }
    if count != 0 { println!("") }
  }

  /// Prints the decisions and propagations at level zero.
  pub fn print_vars_at_0(& self, prefix: & str) {
    log!("{}| ", prefix) ;
    for var in 1..self.var_count() {
      if let VarInfo { value: Some( ref value ), .. } = self[var] {
        if value.lvl() < 2 {
          print!("{}{}@{} ", if value.val {"+"} else {"-"}, var, value.lvl())
        }
      }
    } ;
    println!("|") ;
  }

  /// Prints the weight and the hint of the variables.
  pub fn print_variables_weight(& self, prefix: & str) {
    let max_per_line = 10 ;
    let mut count = 0 ;
    for var in 1..(self.var_count() + 1) {
      if count == 0 {
        log!("{}", prefix)
      } ;
      let VarInfo {
        ref value, hint, weight, ..
      } = self[var] ;
      print!(
        "{}{}({}{}), ",
        match * value {
          None => "?",
          Some( Valuation { val: true, .. } ) => "+",
          Some( Valuation { val: false, .. } ) => "-",
        },
        var,
        if hint {"+"} else {"-"},
        weight
      ) ;
      count += 1 ;
      if count >= max_per_line {
        println!("") ;
        count = 0
      }
    }
    if count != 0 { println!("") }
  }


  /// The number of variables in the instance.
  #[inline]
  pub fn var_count(& self) -> Size {
    self.var_map.len() - 1
  }

  /// Resets the conflict kid counter of a variable to zero.
  #[inline]
  pub fn reset_conflict_kids_of(& mut self, v: Var) {
    self.var_map[v].conflict_kids = 0
  }
  /// Increments the conflict kid counter of a variable.
  #[inline]
  pub fn inc_conflict_kids_of(& mut self, v: Var) {
    self.var_map[v].conflict_kids += 1
  }
  /// Decreases the conflict kid counter of a variable.
  #[inline]
  pub fn dec_conflict_kids_of(& mut self, v: Var) {
    self.var_map[v].conflict_kids -= 1
  }
  /// Returns the conflict kid counter of a variable.
  #[inline]
  pub fn conflict_kids_of(& self, v: Var) -> usize {
    self.var_map[v].conflict_kids
  }

  /// Normalizes the weight of variables if necessary.
  pub fn normalize_var_weight(& mut self) {
    let normalized = self.weight_var_map.normalize() ;
    // self.weight_var_map.force_normalize() ;
    if normalized {
      // logln!("normalizing") ;
      for v in 1..(self.var_count() + 1) {
        self.var_map[v].weight.normalize()
      }
    } // } else { logln!("not normalizing") }
  }


  /// Checks the consistency of the instance.
  pub fn check_consistency(& self) {
    let mut set = VarSet::new(self.var_count() / 2) ;
    for pair in self.weight_var_map.iter() {
      let (ref w, ref s) = * pair ;
      for var in s.iter() {
        let var = * var ;
        if self[var].weight != * w {
          panic!(
            "weight inconsistency on var {}: {}/{}", var, w, self[var].weight
          )
        } ;
        if self[var].value != None {
          panic!(
            "var {} is in weight map but is valuated", var
          )
        } ;
        set.add(var)
      }
    } ;
    for var in 1..self.var_map.len() {
      let var = var ;
      if self[var].value == None {
        if ! set.mem(var) {
          panic!(
            "var {} is undef but is not in weight map"
          )
        }
      }
    }
  }

  /// Iterates on the variables of a clause. Function `action` will be called
  /// on each var in the clause, with arguments the variable and its conflict
  /// kid count.
  /// If `action(var,count)` returns true then the conflict kid count of the
  /// variable will be incremented.
  #[inline]
  pub fn iter_var_of_clause_inc_conflict<F,T>(
    & mut self, clause: CIndex, something: & mut T, action: F
  ) where F: Fn(Var, Size, & mut T) -> bool {
    // Retrieving clause.
    let clause = match clause {
      CIndex::Learnt(index) => & self.learnt[index],
      CIndex::Cnf(index) => & self.cnf[index],
    } ;
    for lit in clause.iter() {
      let var = lit.to_var() ;
      let count = self.var_map[var].conflict_kids ;
      let inc = action(var, count, something) ;
      if inc { self.var_map[var].conflict_kids += 1 }
    }
  }

  /// Iterates on the variables of a clause. Function `action` will be called
  /// on each var in the clause, with arguments the variable, its conflict kid
  /// count and the source of the variable.
  /// If `action(var,count)` returns true then the conflict kid count of the
  /// variable will be decremented.
  #[inline]
  pub fn iter_var_of_clause_dec_conflict<F,T>(
    & mut self, clause: CIndex, something: & mut T, action: F
  ) where F: Fn(Var, Size, Source, & mut T) -> bool {
    // Retrieving clause.
    let clause = match clause {
      CIndex::Learnt(index) => & self.learnt[index],
      CIndex::Cnf(index) => & self.cnf[index],
    } ;
    for lit in clause.iter() {
      let var = lit.to_var() ;
      let count = self.var_map[var].conflict_kids ;
      let source = self.source_of_var(var) ;
      let dec = action(var, count, source, something) ;
      if dec { self.var_map[var].conflict_kids -= 1 }
    }
  }


  /// Adds a new clause to the learnt clauses. Updates its literals.
  pub fn add_learnt(& mut self, var: Var, clause: Vec<Lit>) -> CIndex {
    // for pair in self.weight_var_map.iter() {
    //   let (ref w, ref s) = * pair ;
    //   logln!("> {}: {}", w, s)
    // }
    let index = self.learnt.len() ;
    let cindex = CIndex::Learnt(index) ;
    let clause = Clause::new(clause, cindex) ;
    // logln!("learnt {}", clause) ;
    let mut weight_map = WeightVarMap::new(Weight::pivot()) ;
    let pivot = self.var_count() / 2 ;
    weight_map.update(pivot, self[var].weight, var) ;
    self.weight_var_map.minus(& mut weight_map) ;
    // Update the variables appearing in the clause.
    for lit in clause.iter() {
      let var = lit.to_var() ;
      self.var_map[var].weight.inc() ;
      self.var_map[var].add_learnt(index)
    }
    // Update vector of learnt clauses.
    self.learnt.push(clause) ;
    cindex
  }

  /// Returns the info a variable is associated to.
  #[inline]
  pub fn info_of(& self, v: Var) -> & VarInfo {
    & self.var_map[v]
  }

  /// Returns the info a variable is associated to (mutable version).
  #[inline]
  pub fn mut_info_of(& mut self, v: Var) -> & mut VarInfo {
    & mut self.var_map[v]
  }

  /// Switches the value of a variable.
  ///
  /// **Panics** if the variable is unassigned.
  #[inline]
  pub fn switch(& mut self, var: Var, from: Source) {
    let new_value = match self.var_map[var].value {
      None => panic!("switch of undef variable {}", var),
      Some( Valuation { val, .. } ) => {
        Valuation { val: ! val, from: from }
      },
    } ;
    self.var_map[var].value = Some( new_value ) ;
    // let mut weight_map = WeightVarMap::new(Weight::pivot()) ;
    // weight_map.update(0, self[var].weight, var) ;
    // logln!("switch") ;;
    // logln!("to minus: {}", weight_map) ;
    // self.weight_var_map.minus(& mut weight_map) ;
    // logln!("minused: {}", self.weight_var_map) ;
  }

  /// Returns the decision level of a variable.
  ///
  /// **Panics** if the variable is unassigned.
  #[inline]
  pub fn lvl_of_var(& self, v: Var) -> Lvl {
    match self.var_map[v].value {
      Some( Valuation { from, .. } ) => from.lvl(),
      None => panic!("level of undef var {}", v),
    }
  }

  /// Returns the source of a variable.
  ///
  /// **Panics** if the variable is unassigned.
  #[inline]
  pub fn source_of_var(& self, v: Var) -> Source {
    match self.var_map[v].value {
      Some( Valuation { from, .. } ) => from,
      None => panic!("source of undef var {}", v),
    }
  }

  /// Returns the source of a literal.
  ///
  /// **Panics** if the literal is unassigned.
  #[inline]
  pub fn source_of_lit(& self, l: Lit) -> Source {
    match self.var_map[l.to_var()].value {
      Some( Valuation { from, .. } ) => from,
      None => panic!("source of undef lit {}", l),
    }
  }

  /// Returns the value of a variable.
  #[inline]
  pub fn val_of_var(& self, v: Var) -> Option<bool> {
    match self.var_map[v] {
      VarInfo { value: Some(ref val), .. } => Some(val.val),
      _ => None,
    }
  }

  /// Returns the value of a literal.
  #[inline]
  pub fn val_of_lit(& self, l: Lit) -> Option<bool> {
    if l.l >= 0 { self.val_of_var( l.l as Var ) }
    else {
      match self.var_map[ (-l.l) as Var ] {
        VarInfo { value: Some(ref val), .. } => Some(!val.val),
        _ => None,
      }
    }
  }

  /// Returns the value of a variable.
  ///
  /// **Panics** if the variable is unassigned.
  #[inline]
  pub fn unsf_val_of_var(& self, v: Var) -> bool {
    match self.var_map[v] {
      VarInfo { value: Some(ref val), .. } => val.val,
      _ => panic!("value of undef var {}", v),
    }
  }
  /// Returns the value of a literal.
  ///
  /// **Panics** if the literal is unassigned.
  #[inline]
  pub fn unsf_val_of_lit(& self, l: Lit) -> bool {
    if l.l >= 0 { self.unsf_val_of_var( l.l as Var ) }
    else { ! self.unsf_val_of_var( (-l.l) as Var ) }
  }

  /// Returns the literal corresponding to `v` which is `false` with the
  /// current valuation of `v`.
  ///
  /// Used in lemma construction during conflict resolution.
  ///
  /// **Panics** if the literal is unassigned.
  #[inline]
  pub fn neg_lit_of_var(& self, v: Var) -> Lit {
    match self.var_map[v] {
      VarInfo { value: Some(ref val), .. } => Lit::neg_of_val(v, val),
      _ => panic!("negated literal of undefined variable"),
    }
  }

  /// Pops a singleton clause.
  #[inline]
  pub fn pop_singleton(& mut self) -> Option<Lit> {
    self.singleton_clauses.pop()
  }

  /// Pops the heaviest variable from the weight to variable map.
  #[inline]
  pub fn pop_heaviest_var(& mut self) -> Option<Var> {
    // logln!("popping heaviest") ;
    match self.weight_var_map.pop_heaviest() {
      Some(var) => { Some(var) }, // logln!("> {}", var) ; Some(var) },
      None => None,
    }
  }

  /// Returns the status of the instance by propagating the value of a
  /// variable. More precisely, returns
  ///
  /// * `Some(v,i)` - if a conflict arose because of the value of variable `v`
  ///   in clause `i`.
  /// * `None` - otherwise.
  ///
  pub fn propagate(& mut self, lvl: Lvl, var: Var) -> Option<(Var, CIndex)> {
    let mut to_update = Vec::with_capacity(10) ;
    // self.update_clauses( var, & mut to_update ) ;
    let mut var = var ;
    let mut weight_map = WeightVarMap::new(Weight::pivot()) ;
    let pivot = self.var_count() / 2 ;
    loop {
      weight_map.update(pivot, self[var].weight, var) ;
      match self.update_clauses( var, & mut to_update ) {
        CnfStatus::Conflict(clause) => return Some((var,clause)),
        _ => (),
      } ;
      if let Some((lit, from, clause)) = to_update.pop() {
        self.stats.assignments_inc() ;
        let from = Source::Propagation(lvl, from, clause) ;
        let (var_to_propagate, valuation) = lit.to_val(from) ;
        var = var_to_propagate ;
        self.var_map[var].value = Some( valuation ) ;
      } else { break }
    } ;
    self.weight_var_map.minus(& mut weight_map) ;
    None
  }

  /// Update clauses mentioning `var`. Returns the new status of the cnf.
  fn update_clauses(
    & mut self, var: Var, vars: & mut Vec<(Lit, Var, CIndex)>
  ) -> CnfStatus {
    for clause_i in self.var_map[var].clause_iter() {
      let mut clause = match clause_i {
        CIndex::Cnf(ref i) => & mut self.cnf[* i],
        CIndex::Learnt(ref i) => & mut self.learnt[* i],
      } ;
      // logln!("> clause: {}", clause) ;
      let status = clause.update(& self.var_map) ;
      match status {
        // Undef, skipping.
        CStatus::Undef => (),
        // Unit, making new valuation, updating variables to update.
        CStatus::Unit(lit) => vars.push((lit, var, clause_i)),
        // Unsat, done.
        CStatus::Unsat => {
          // logln!("> conflict") ;
          return CnfStatus::Conflict( clause_i )
        },
        CStatus::Sat => (),
      }
    } ;
    CnfStatus::Undef
  }

  /// Undoes all valuations of decisions of level higher than `lvl`, resets
  /// conflict kids counters to zero. Returns the decision variable
  /// corresponding to `lvl`.
  pub fn backjump(& mut self, lvl: Lvl) {
    // logln!("") ;
    // logln!("Backjump to level {}:", lvl) ;
    // logln!("  {}", self) ;
    // logln!("Weights:") ;
    // self.print_variables_weight("  ") ;
    // logln!("Weight map:") ;
    // for pair in self.weight_var_map.iter() {
    //   let (ref w, ref s) = * pair ;
    //   logln!("> {}: {}", w, s)
    // }
    let mut to_merge = self.var_map.backjump(lvl) ;
    // logln!("to merge: {}", to_merge) ;
    self.weight_var_map.merge(& mut to_merge) ;
    // logln!("merged: {}", self.weight_var_map) ;
    // logln!("instance:") ;
    // logln!("  {}", self) ;
    // logln!("checking consistency after backjump") ;
    // self.check_consistency() ;
  }

  /// Returns true iff the current assignment satisfies the cnf.
  pub fn is_model(& self) -> bool {
    for clause in self.cnf.iter() {
      let mut clause_sat = false ;
      for lit in clause.iter() {
        if self.var_map.val_of_lit(* lit) == Some(true) {
          clause_sat = true ;
          break
        }
      } ;
      if ! clause_sat {
        log!("not sat: {}", clause) ;
        return false
      }
    } ;
    return true
  }


  /// Prints the current valuations, their decision level, and the variable
  /// and clause they come from.
  pub fn explain(& self, prefix: & str) {
    for var in 1..(self.var_map.len()) {
      if let VarInfo { value: Some( ref val ), .. } = self[var] {
        log!(
          "{}{}{}{}", prefix,
          if val.val {"+"} else {"-"}, var, val.from
        ) ;
        match val.from {
          Source::Propagation(_,_,clause) =>
            log!("{}   {}", prefix, self[clause]),
          _ => (),
        }
      }
    }
  }
}

impl ops::Index<Var> for Instance {
  type Output = VarInfo ;
  fn index(& self, var: Var) -> & VarInfo {
    & self.var_map[var]
  }
}

impl ops::Index<CIndex> for Instance {
  type Output = Clause ;
  fn index(& self, index: CIndex) -> & Clause {
    match index {
      CIndex::Cnf(i) => & self.cnf[i],
      CIndex::Learnt(i) => & self.learnt[i],
    }
  }
}



impl fmt::Display for Instance {
  fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
    // try!( write!(fmt, "cnf{}[", self.cnf.len()) ) ;
    if self.var_map.len() > 1 {
      match self.var_map[1] {
        VarInfo { value: None, .. } => try!( write!(
          fmt, "?{}", 1
        ) ),
        VarInfo {
          value: Some( Valuation { val, from } ), ..
        } => try!( write!(
          fmt, "{}{}{}", if val {"+"} else {"-"}, 1, from
        ) ),
      } ;
      for var in 2..self.var_map.len() {
        match self.var_map[var] {
          VarInfo { value: None, .. } => try!( write!(
            fmt, " ?{}", var
          ) ),
          VarInfo {
            value: Some( Valuation { val, from } ), ..
          } => try!( write!(
            fmt, " {}{}{}", if val {"+"} else {"-"}, var, from
          ) ),
        }
      }
    } ;
    Ok(())
  }
}



// |===| Error things.



static no_lines: & 'static str =
  "no lines to create an instance from" ;
static size_mismatch: & 'static str =
  "dimension mismatch between header and cnf" ;

static open_failed: & 'static str =
  "could not open file" ;



// |===| Helper things.


/// Type of a function creating an instance.
type InstanceMaker<T> = fn(
  & Conf, Vec<Clause>, Vec<(Vec<Index>, Size)>, Vec<Lit>
) -> T ;


/// Creates an instance from an iterator on some lines.
fn of_lines<T,Res>(
  conf: & Conf, lines: & mut T, new_instance: InstanceMaker<Res>
) -> Result<Res, & 'static str>
where T: Iterator<Item = String> {

  match lines.next() {
    None => Err( no_lines ),

    Some(header) => {
      let (var_count, mut size) = info_of_header( header ) ;

      let mut cnf = Vec::with_capacity(size) ;
      let mut var_map = vec![(Vec::new(),0) ; var_count + 1] ;
      let mut singleton_clauses = vec![] ;

      // Actual index, not incremented for singleton clauses.
      let mut index = 0 ;
      for _ in 0..size { match lines.next() {
        None => return Err( size_mismatch ),

        Some(line) => {
          let cindex = CIndex::Cnf(index) ;
          // Building clause.
          match Clause::of_str( & line, cindex ) {
            Ok(clause) => {
              // Updating var map.
              for lit in clause.iter() {
                let var = lit.to_var() ;
                var_map[ var ].0.push(index) ;
                if lit.l < 0 { var_map[ var ].1 += 1 } ;
              } ;
              // Incrementing the actual index.
              index += 1 ;
              // Updating cnf.
              cnf.push(clause)
            },
            Err(lit) => {
              // Singleton clauses do not count as clauses.
              size -= 1 ;
              singleton_clauses.push(lit)
            },
          }
        },
      } } ;

      Ok( new_instance(
        conf, cnf, var_map, singleton_clauses,
      ) )
    }
  }
}

/// Creates an instance from a path to a file.
fn of_path<Res>(
  conf: & Conf, path : & Path, new_instance: InstanceMaker<Res>
) -> Result<Res, & 'static str> {

  match File::open(path) {
    Err(_) => Err( open_failed ),
    Ok(file) => {
      let buf_reader = BufReader::new( file ) ;
      let mut iterator = buf_reader.lines().filter_map(
        |line_res| match line_res {
          Ok( line ) =>
            if line.starts_with("c") ||
               line.starts_with("%") ||
               line.is_empty() {
              None
            } else { Some(line) },
          _ => None,
        }
      ).into_iter() ;
      of_lines( conf, & mut iterator, new_instance )
    }
  }
}


/// Parses the header of a SAT cnf problem.
fn info_of_header( s: String ) -> (Size, Size) {
  let mut tokens = s.trim().split(" ").filter(|s| ! s.is_empty()) ;
  match tokens.next() {
    Some("p") => (),
    _ => panic!(format!("unexpected header \"{}\"", s)),
  } ;
  match tokens.next() {
    Some("cnf") => (),
    _ => panic!(format!("unexpected header \"{}\"", s)),
  } ;

  let var_count = match tokens.next().unwrap().parse::<Size>() {
    Ok(count) => count,
    Err(s) => panic!( "\"{}\" when parsing variable count (u32)", s ),
  } ;
  let size = match tokens.next().unwrap().parse::<Size>() {
    Ok(size) => size,
    Err(s) => panic!( "\"{}\" when parsing cnf size (u32)", s ),
  } ;
  (var_count, size)
}



// |===| Display things.




// impl<A> fmt::Display for Instance<A>
// where A: Assignment + Sized + fmt::Display {
//   fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
//     write!(
//       fmt, "{{ c:{} | a:{} / v:{} }}",
//       self.len(), self.assign_len(), self.var_count(),
//     )
//   }
// }





// |===| Test things.




// #[test]
// pub fn from_path() {
//   use std::fs::read_dir ;

//   let files = read_dir(& Path::new("rsc/uf20/")).unwrap() ;
//   let conf = Conf::default_conf() ;

//   for file in files {
//     let path = file.ok().unwrap().path() ;
//     let path = path.as_path() ;
//     if let Some( ext ) = path.extension() {
//       if ext.to_str().unwrap() == "cnf" {
//         match Instance::of_path( path, & conf ) {
//           Err(s) => {
//             println!("Error on {:?}: {}", path, s) ;
//             assert!(false)
//           },
//           Ok(_) => assert!(true),
//         }
//       }
//     }
//   }

// }





// #[test]
// pub fn from_path_new() {
//   use std::fs::read_dir ;

//   let files = read_dir(& Path::new("rsc/uf20/")).unwrap() ;
//   let conf = Conf::default_conf() ;

//   for file in files {
//     let path = file.ok().unwrap().path() ;
//     let path = path.as_path() ;
//     if let Some( ext ) = path.extension() {
//       if ext.to_str().unwrap() == "cnf" {
//         match NewInstance::of_path( path, & conf ) {
//           Err(s) => {
//             println!("Error on {:?}: {}", path, s) ;
//             assert!(false)
//           },
//           Ok(_) => assert!(true),
//         }
//       }
//     }
//   }

// }

// #[test]
// fn unsat_one_step() {

//   let path = "rsc/unsat_one_step.cnf" ; // "rsc/uf20-91/uf20-01.cnf"
//   let path = Path::new( path ) ;
//   let conf = Conf::default_conf() ;
//   println!("Loading cnf from [{}].\n", path.to_str().unwrap()) ;

//   match Instance::of_path( path, & conf ) {
//     Err(s) => println!("Error: {}", s),
//     Ok(mut instance) => {
//       println!("Ok: {}", instance) ;
//       let valuation = Valuation::new(1, true) ;
//       println!("Valuation {}", valuation) ;
//       let status = (& mut instance as & mut Propagate).propagate(valuation) ;
//       println!("Got {}", status) ;
//       assert!( status == CnfStatus::Conflict(3) )
//     },
//   } ;
// }
