#![ doc = "
"]

use std::fmt ;


/// A series for the restart strategy.
pub trait Series: Sized + fmt::Display {
  /// The next value in the series.
  fn next(& mut self) -> usize ;
}

/// The Luby series as described in [the 1993 paper][luby paper].
/// [luby paper]: http://www.sciencedirect.com/science/article/pii/0020019093900299 (Luby series 1993 paper)
pub struct Luby {
  /// True iff k was just reset.
  first: bool,
  /// Value used to generate the next power of two.
  k: u32,
  /// The maximum value `k` can reach before it is reset to zero and
  /// `max_k` is incremented.
  max_k: u32,
}

impl Luby {
  /// Creates a new Luby series.
  fn new() -> Luby {
    Luby { first: true, k: 0, max_k: 1 }
  }
}

impl Series for Luby {
  fn next(& mut self) -> usize {
    if self.first {
      self.first = false ;
      1
    } else {
      let next = (2 as usize).pow(self.k) ;
      if self.k < self.max_k { self.k += 1 }
      else { self.max_k += 1 ; self.k = 0 ; self.first = true }
      next
    }
  }
}

impl fmt::Display for Luby {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    let val = if self.first {1} else {(2 as usize).pow(self.k)} ;
    write!( fmt, "luby({} | {},{},{})", val, self.first, self.k, self.max_k )
  }
}


/// A restart strategy.
pub struct RestartStrat<S: Series> {
  /// The series the strategy uses.
  series: S,
  /// The current index in the series.
  current: usize,
}

impl<S: Series> RestartStrat<S> {
  /// Creates a restart strategy using the Luby series.
  pub fn luby() -> RestartStrat<Luby> {
    let mut luby = Luby::new() ;
    let current = luby.next() ;
    RestartStrat { series: luby, current: current }
  }

  /// Decrements the current value of the restart strategy and returns true
  /// if it reaches zero.
  /// Should be called after conflict resolution to restart or not.
  pub fn should_restart(& mut self) -> bool {
    self.current -= 1 ;
    if self.current == 0 {
      self.current = self.series.next() ; true
    } else { false }
  }
}

impl<S: Series> fmt::Display for RestartStrat<S> {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "{} || {}", self.current, self.series)
  }
}