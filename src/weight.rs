#![ doc = "
"]

use std::fmt ;

use ::common::{Var, CIndex} ;
use ::sorted::{Map, Set} ;


/// The constant used to divide weights when normalizing.
pub static normalization_constant: u8 = 10u8 ;
/// Value above which the weights should be normalized.
pub static normalize_threshold: u8 = 200u8 ;

/// A wrapper around a `u8`.
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Weight { w: u8 }

impl Weight {

  /// Creates a default weight (`0`).
  pub fn new() -> Self { Weight { w: 0 } }

  /// The pivot weight used by weight maps. Corresponds to half the
  /// normalization threshold.
  pub fn pivot() -> Self { Weight { w: normalize_threshold / 2 } }

  /// Creates a new weight from a value.
  pub fn of_u8(w: u8) -> Self { Weight { w: w } }

  /// Divides the value of a weight by `normalization_constant`.
  #[inline]
  pub fn normalize(& mut self) {
    self.w = self.w / normalization_constant ;
  }

  /// Increments a weight.
  #[inline]
  pub fn inc(& mut self) {
    if self.w < 255u8 { self.w += 1 }
  }

  /// Returns true if this weight is above the normalization threshold.
  pub fn should_normalize(& self) -> bool {
    self.w >= normalize_threshold
  }
}


/// Maps weights to sets of variables.
pub type WeightVarMap = ::sorted::Map<Weight, ::sorted::Set<Var>> ;

/// Creates a new weight to variable map.
pub fn weight_var_map(last_var: Var) -> WeightVarMap {
  WeightVarMap::new_weight_var_map(last_var)
}

/// Maps weights to sets of clause indices.
pub type WeightClauseMap = Map<Weight, Set<CIndex>> ;


// |===| Display things.

impl fmt::Display for Weight {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "w{}", self.w)
  }
}



// |===| Test things.

#[test]
fn weight_map() {
  fn test_norm(map: & WeightVarMap, res: bool) {
    let norm = map.should_normalize() ;
    println!("should norm: {}", norm) ;
    assert_eq!( norm, res )
  }

  let mut map = weight_var_map(10) ;
  println!("map: {}", map) ;
  assert_eq!(
    Set::of_vec(5, vec![1,2,3,4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;

  let mut inc = weight_var_map(3) ;
  println!("inc: {}", inc) ;

  map.increment(& mut inc) ;
  println!("map: {}", map) ;
  assert_eq!(
    Set::of_vec(5, vec![4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![1,2,3]),
    map.get(Weight::of_u8(1u8)).unwrap().clone()
  ) ;

  let mut inc = weight_var_map(2) ;
  inc.increment(& mut weight_var_map(2)) ;
  println!("inc: {}", inc) ;

  map.increment(& mut inc) ;
  println!("map: {}", map) ;
  test_norm(& map, false) ;
  assert_eq!(
    Set::of_vec(5, vec![4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![3]),
    map.get(Weight::of_u8(1u8)).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![1,2]),
    map.get(Weight::of_u8(2u8)).unwrap().clone()
  ) ;

  println!("") ;

  let highest = map.pop_heaviest().unwrap() ;
  println!("pop highest: {}", highest) ;
  println!("map: {}", map) ;
  assert_eq!( highest, 1 ) ;
  assert_eq!(
    Set::of_vec(5, vec![4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![3]),
    map.get(Weight::of_u8(1u8)).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![2]),
    map.get(Weight::of_u8(2u8)).unwrap().clone()
  ) ;

  let highest = map.pop_heaviest().unwrap() ;
  println!("pop highest: {}", highest) ;
  println!("map: {}", map) ;
  assert_eq!( highest, 2 ) ;
  assert_eq!(
    Set::of_vec(5, vec![4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![3]),
    map.get(Weight::of_u8(1u8)).unwrap().clone()
  ) ;

  let highest = map.pop_heaviest().unwrap() ;
  println!("pop highest: {}", highest) ;
  println!("map: {}", map) ;
  assert_eq!( highest, 3 ) ;
  assert_eq!(
    Set::of_vec(5, vec![4,5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;

  let highest = map.pop_heaviest().unwrap() ;
  println!("pop highest: {}", highest) ;
  println!("map: {}", map) ;
  assert_eq!( highest, 4 ) ;
  assert_eq!(
    Set::of_vec(5, vec![5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;

  test_norm(& map, false) ;

  println!("") ;

  let mut merge = WeightVarMap::new(Weight::pivot()) ;
  let s1 = Set::of_vec( 5, vec![1,2] ) ;
  let s2 = Set::of_vec( 5, vec![3] ) ;
  let s3 = Set::of_vec( 5, vec![4] ) ;

  merge.add_set(Weight::of_u8(150u8), & s1) ;
  println!("merge: {}", merge) ;
  merge.add_set(Weight::of_u8(153u8), & s2) ;
  println!("merge: {}", merge) ;
  merge.add_set(Weight::of_u8(210u8), & s3) ;
  println!("merge: {}", merge) ;

  println!("") ;

  map.merge(& mut merge) ;
  println!("map: {}", map) ;
  assert_eq!(
    Set::of_vec(5, vec![5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![1,2]),
    map.get(Weight::of_u8(150u8)).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![3]),
    map.get(Weight::of_u8(153u8)).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![4]),
    map.get(Weight::of_u8(210u8)).unwrap().clone()
  ) ;


  test_norm(& map, true) ;

  println!("") ;

  map.normalize() ;
  println!("map: {}", map) ;
  assert_eq!(
    Set::of_vec(5, vec![5,6,7,8,9,10]),
    map.get(Weight::new()).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![1,2,3]),
    map.get(Weight::of_u8(15u8)).unwrap().clone()
  ) ;
  assert_eq!(
    Set::of_vec(5, vec![4]),
    map.get(Weight::of_u8(21u8)).unwrap().clone()
  ) ;

  test_norm(& map, false)
}