#![ doc = "
Defines the `log` and `log_res` macro which are always active, and others
doing nothing unless some features are active.

## Conventions ##

By default the log macros end with a line break. However, if the first argument
of a log macro is `nlb` then no line break will be printed at the end of the
line.
"]


// |===| Log macros.

/// Logs using the `log_prefix` specified in `conf`. If the argument of the
/// macro starts with `nlb` then there will be no line break at the end of the
/// print.
#[macro_export]
macro_rules! log {
  (nlb, $e:expr) => (
    print!("{}{}", ::conf::log_prefix, $e)
  ) ;
  (nlb, $( $e:expr ),+ $(,)*) => (
    print!("{}{}", ::conf::log_prefix, format!($($e),+))
  ) ;
  ($e:expr) => (
    println!("{}{}", ::conf::log_prefix, $e)
  ) ;
  ($( $e:expr ),+ $(,)*) => (
    println!("{}{}", ::conf::log_prefix, format!($($e),+))
  ) ;
}

/// Logs using the `log_res_prefix` specified in `conf`. Same line break
/// convention as `log`.
#[macro_export]
macro_rules! log_res {
  (nlb, $e:expr) => (
    print!("{}{}", ::conf::log_res_prefix, $e)
  ) ;
  (nlb, $( $e:expr ),+ $(,)*) => (
    print!("{}{}", ::conf::log_res_prefix, format!($($e),+))
  ) ;
  ($e:expr) => (
    println!("{}{}", ::conf::log_res_prefix, $e)
  ) ;
  ($( $e:expr ),+ $(,)*) => (
    println!("{}{}", ::conf::log_res_prefix, format!($($e),+))
  ) ;
}


// |===| Conditional (at compile time) log macros.



/// Log level 1, activated by `log1`.
#[cfg(any(
  feature = "log1",
  feature = "log2",
  feature = "log3",
))]
#[macro_export]
macro_rules! log1 {
  ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
}
/// Log level 1, activated by `log1`.
#[cfg(not(any(
  feature = "log1",
  feature = "log2",
  feature = "log3",
)))]
#[macro_export]
macro_rules! log1 {
  ($( $es:expr ),+ ) => ( () ) ;
}

/// Log level 2, activated by `log2`.
#[cfg(any(
  feature = "log2",
  feature = "log3",
))]
#[macro_export]
macro_rules! log2 {
  ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
}
/// Log level 2, activated by `log2`.
#[cfg(not(any(
  feature = "log2",
  feature = "log3",
)))]
#[macro_export]
macro_rules! log2 {
  ($( $es:expr ),+ ) => ( () ) ;
}

/// Log level 3, activated by `log3`.
#[cfg(any(
  feature = "log3",
))]
#[macro_export]
macro_rules! log3 {
  ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
}
/// Log level 3, activated by `log3`.
#[cfg(not(any(
  feature = "log3",
)))]
#[macro_export]
macro_rules! log3 {
  ($( $es:expr ),+ ) => ( () ) ;
}

// /// Logs events in the DPLL algorithm, activated by `log_dpll`.
// #[cfg(any(
//   feature = "log_dpll"
// ))]
// #[macro_export]
// macro_rules! log_dpll {
//   ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
// }
// /// Logs events in the DPLL algorithm, activated by `log_dpll`.
// #[cfg(not(any(
//   feature = "log_dpll"
// )))]
// #[macro_export]
// macro_rules! log_dpll {
//   ($( $es:expr ),+ ) => ( () ) ;
// }

// /// Logs resolution events outside of coloring and uip extraction. Activated by
// /// `log_resolution`, `log_coloring`, and `log_uip`.
// #[cfg(any(
//   feature = "log_resolution_lw",
//   feature = "log_resolution",
//   feature = "log_coloring",
//   feature = "log_uip"
// ))]
// #[macro_export]
// macro_rules! log_resolve {
//   ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
// }
// /// Logs resolution events outside of coloring and uip extraction. Activated by
// /// `log_resolution_lw`, `log_resolution`, `log_coloring`, and `log_uip`.
// #[cfg(not(any(
//   feature = "log_resolution_lw",
//   feature = "log_resolution",
//   feature = "log_coloring",
//   feature = "log_uip"
// )))]
// #[macro_export]
// macro_rules! log_resolve {
//   ($( $es:expr ),+ ) => ( () ) ;
// }

// /// Logs coloring events, activated by `log_resolution` and `log_coloring`.
// #[cfg(any(
//   feature = "log_resolution",
//   feature = "log_coloring"
// ))]
// #[macro_export]
// macro_rules! log_color {
//   ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
// }
// /// Logs coloring events, activated by `log_resolution` and `log_coloring`.
// #[cfg(not(any(
//   feature = "log_resolution",
//   feature = "log_coloring"
// )))]
// #[macro_export]
// macro_rules! log_color {
//   ($( $es:expr ),+ ) => ( () ) ;
// }

// /// Logs uip extraction events, activated by `log_resolution` and `log_uip`.
// #[cfg(any(
//   feature = "log_resolution",
//   feature = "log_uip"
// ))]
// #[macro_export]
// macro_rules! log_uip {
//   ($( $es:expr ),+ ) => ( log!($( $es ),+) ) ;
// }
// /// Logs uip extraction events, activated by `log_resolution` and `log_uip`.
// #[cfg(not(any(
//   feature = "log_resolution",
//   feature = "log_uip"
// )))]
// #[macro_export]
// macro_rules! log_uip {
//   ($( $es:expr ),+ ) => ( () ) ;
// }



// |===| Assertions.


/// Assertion macro, activated by `assert`. Usage:
///
/// * `assertion!(eq <lhs>, <rhs>)` for `assert_eq!(<lhs>, <rhs>)`
/// * `assertion!(run <statement>)` for `<statement>`
/// * `assertion!(<bool_expr>)` for `assert!(<bool_expr>)`
#[cfg( feature = "assert" )]
#[macro_export]
macro_rules! assertion {
  ( eq $lhs:expr, $rhs:expr ) => ( assert_eq!( $lhs, $rhs ) ) ;
  ( run $s:stmt ) => ( $s ) ;
  ( $e:expr ) => ( assert!( $e ) ) ;
}
/// Assertion macro, activated by `assert`. Usage:
///
/// * `assertion!(holds <bool_expr>)` for `assert!(<bool_expr>)`
/// * `assertion!(eq <lhs>, <rhs>)` for `assert_eq!(<lhs>, <rhs>)`
/// * `assertion!(run <statement>)` for `<statement>`
#[cfg(not( feature = "assert" ))]
#[macro_export]
macro_rules! assertion {
  ( eq $lhs:expr, $rhs:expr ) => () ;
  ( run $e:stmt ) => () ;
  ( $e:expr ) => () ;
}