#![ doc = "
"]

use std::env::args ;
use std::str::FromStr ;

use ::info::*;


/// Exit status if sat.
pub static sat_exit_status: i32 = 10 ;
/// Exit status if unsat.
pub static unsat_exit_status: i32 = 20 ;
/// Exit status if timeout.
pub static timeout_exit_status: i32 = 0 ;
/// Exit status if error.
pub static error_exit_status: i32 = 2 ;
/// Prefix for all log lines.
pub static log_prefix: & 'static str = "c " ;
/// Prefix the result line.
pub static log_res_prefix: & 'static str = "s " ;


/// Has a value, is printable and can parse.
pub trait ConfItem {
  /// The type of this configuration item.
  type Type ;
  /// The value of an item.
  fn get(& self) -> <Self as ConfItem>::Type ;
  /// Print usage of an item.
  fn print_usage(& self) ;
  /// If `option` matches one of the flags of this item, updates itself
  /// possibly consuming some elements of `iter`. Returns `Ok(true)` if the
  /// update was successful, `Ok(false)` if `option` does not match, and
  /// `Err(...)` if `option` matched but update failed.
  fn parse(
    & mut self, option: & String, iter: & mut Iterator<Item=String>
  ) -> Result<bool,String> ;
}

macro_rules! mk_conf_item {
  (
    $flg:ident, $t:ty, $t_str:expr, $d:expr,
    flags ( $flag:expr, $( $flags:expr ),*),
    description ( $( $descs:expr ),+ ),
    parse ($slf:ident, $iter:ident -> $parse:block),
    print default $print_default:expr
  ) => (
    /// The $flg configuration item.
    struct $flg {
      /// The value stored in this item.
      value: $t,
      /// The flags of this item.
      flags: Vec<String>
    }
    impl $flg {
      /// Returns the default value of the $flg item.
      fn default() -> $t { $d }
      /// Creates a new $flg.
      pub fn new() -> $flg {
        $flg {
          value: $flg::default(),
          flags: vec![
            $flag.to_string(), $($flags.to_string()),*
          ],
        }
      }
    }
    impl ConfItem for $flg {
      type Type = $t ;
      fn get(& self) -> $t { self.value }
      fn print_usage(& self) {
        log!(nlb, "{}", $flag) ;
        $( print!(", {}", $flags) ; )*
        if $print_default { println!(" <{}> [{}]", $t_str, $d) }
        else { println!("") }
        $( log!("   {}", $descs) ; )+
      }
      #[allow(unused_variables)]
      fn parse(
        & mut $slf, option: & String, $iter: & mut Iterator<Item=String>
      ) -> Result<bool,String> {
        for flag in $slf.flags.iter() {
          if flag == option {
            match $parse {
              None => return Ok(true),
              Some(err) => return Err(
                format!("on {}: {}", option, err)
              ),
            }
          }
        } ;
        return Ok(false)
      }
    }
  ) ;
}

macro_rules! mk_conf {
  ( $( $flg:ident as $id:ident: $t:ty ),+ ) => (
    /// Configuration of a satire run.
    pub struct Conf {
      files: Vec<String>,
      errors: Vec<String>,
      $( $id : $flg ),+
    }
    impl Conf {
      /// Attempts to create a new configuration by parsing the command line
      /// arguments. Prints usage and errors if necessary and returns an error
      /// of the exit status in case of failure.
      pub fn new() -> Result<Conf,i32> {
        let mut conf = Conf {
          files: vec![],
          errors: vec![],
          $( $id: $flg::new() ),+
        } ;
        let mut args = args() ;
        let _ = args.next() ;
        conf.print_version() ;
        conf.parse(& mut args) ;
        if ! conf.version() { log!("") } ;
        match (conf.help(), conf.version(), conf.errors.is_empty()) {
          (true, _, _) => {
            conf.print_usage() ;
            Err(timeout_exit_status)
          },
          (_, true, _) => Err(timeout_exit_status),
          (_, _, false) => {
            conf.error() ;
            Err(error_exit_status)
          },
          _ => Ok(conf),
        }
      }

      fn parse(
        & mut self, iter: & mut Iterator<Item=String>
      ) {

        loop {
          if let Some(next) = iter.next() {
            if next.starts_with("-") {
              $(
                let has_parsed = match self.$id.parse(& next, iter) {
                  Ok(b) => b,
                  Err(msg) => {
                    self.errors.push(msg) ;
                    true
                  },
                } ;
                if has_parsed { continue } ;
              )+
              self.errors.push(
                format!("unknown option {}", next)
              )
            } else {
              // `next` is not an option.
              self.files.push(next) ;
              for file in iter {
                self.files.push(file)
              } ;
              break
            }
          } else {
            self.errors.push(
              "no file specified".to_string()
            ) ;
            break
          }
        }
      }

      /// Default configuration for tests.
      #[cfg(test)]
      pub fn default_conf() -> Conf {
        Conf {
          files: vec![],
          errors: vec![],
          $( $id: $flg::new() ),+
        }
      }

      fn print_version(& self) {
        log!("SatIre v{}", version) ;
        log!(
          "  from branch {} #{} ({})", git_branch, git_short_hash, git_hash
        ) ;
        if git_modified.len() > 0 {
          if git_modified[0] != "" {
            log!(
              "  file{} modified:", if git_modified.len() > 1 {"s"} else {""}
            ) ;
            log!(nlb, "    ") ;
            print!("\"{}\"", git_modified[0]) ;
            for file in git_modified.iter().skip(1) {
              print!(", \"{}\"", file)
            } ;
            println!("")
          }
        }
      }

      fn print_usage(& self) {
        log!("Usage: satire [<option>]* [<file>]+") ;
        log!("   where <option> is one of the following:") ;
        log!("") ;
        $( self.$id.print_usage() ; )+
      }

      fn error(& self) {
        self.print_usage() ;
        log!("") ;
        log!("Error{}:", if self.errors.len() > 1 { "s" } else { "" }) ;
        for line in self.errors.iter() {
          log!("|  {}", line)
        } ;
        log!("") ;
      }

      /// The files to run on.
      pub fn files<'a>(& 'a self) -> & 'a Vec<String> {
        & self.files
      }

      /// Sets a timeout, in test only.
      #[cfg(test)]
      pub fn set_timeout(& mut self, to: usize) {
        self.timeout.value = to
      }

      $(
        /// Flag value (macro-generated).
        pub fn $id(& self) -> $t {
          self.$id.get()
        }
      )+
    }
  ) ;
}



// |===| Command line argument handlers.


mk_conf_item!(
  Help, bool, "bool", false,
  flags ("-h", "--help"),
  description ("Prints this help message."),
  parse (self, iter -> {
    self.value = true ;
    none_as_string_option()
  }),
  print default false
) ;

mk_conf_item!(
  Version, bool, "bool", false,
  flags ("-v", "--version"),
  description ("Prints the version of this build of satire."),
  parse (self, iter -> {
    self.value = true ;
    none_as_string_option()
  }),
  print default false
) ;

mk_conf_item!(
  PrintStats, bool, "bool", true,
  flags ("-ps", "--print_stats"),
  description ("Activates printing of statistics when done."),
  parse (self, iter -> {
    if let Some(value) = iter.next() {
      parse_bool_set(& mut self.value, & value)
    } else {
      Some(expected_str("bool", "eol"))
    }
  }),
  print default true
) ;


mk_conf_item!(
  Timeout, usize, "uint", 0,
  flags ("-to", "--timeout"),
  description (
    "Sets a timeout for the analysis (0 for none).",
    ""
  ),
  parse (self, iter -> {
    if let Some(value) = iter.next() {
      parse_usize_set(& mut self.value, & value)
    } else {
      Some(expected_str("uint", "eol"))
    }
  }),
  print default true
) ;

mk_conf_item!(
  Model, bool, "bool", false,
  flags ("-m", "--model"),
  description ("Activates model printing if instance is sat."),
  parse (self, iter -> {
    if let Some(value) = iter.next() {
      parse_bool_set(& mut self.value, & value)
    } else {
      Some(expected_str("bool", "eol"))
    }
  }),
  print default true
) ;

mk_conf_item!(
  ModelCheck, bool, "bool", false,
  flags ("-cm", "--check_model"),
  description ("Model will be checked if instance is sat."),
  parse (self, iter -> {
    if let Some(value) = iter.next() {
      parse_bool_set(& mut self.value, & value)
    } else {
      Some(expected_str("bool", "eol"))
    }
  }),
  print default true
) ;



// |===| Registering command line arguments.


mk_conf!(
  Help as help: bool,
  Version as version: bool,
  PrintStats as print_stats: bool,
  Timeout as timeout: usize,
  Model as model: bool,
  ModelCheck as model_check: bool
) ;



// |===| Helper things.


fn none_as_string_option() -> Option<String> { None }

fn expected_str(exp: & str, found: & str) -> String {
  format!("expected <{}> but found \"{}\"", exp, found)
}

fn parse_usize_set(target: & mut usize, s: & str) -> Option<String> {
  match isize::from_str(s) {
    Ok(v) if v >= 0 => { * target = v as usize ; None },
    _ => Some(expected_str("uint", s)),
  }
}

fn parse_bool_set(target: & mut bool, s: & str) -> Option<String> {
  match bool::from_str(s) {
    Ok(b) => { * target = b ; None },
    _ => Some(expected_str("uint", s)),
  }
}

