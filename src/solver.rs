#![doc = "

# Conflict resolution #

Conflict resolution operates on the conflict graph where nodes are
variables. There is an edge from `v1` to `v2` iff during the value for
`v2` was decided (during propagation) by a unit clause mentioning `v1`.
At the very bottom of the conflict graph is conflict variable `bot_var`
and at the top is the decision at the current level `lvl`. At the
periphery of the graph are decisions and propagations at levels lower
than `lvl`.

The resolution itself consists of three steps:

* node coloring, bottom-up traversal of all nodes,
* uip extraction, bottom-up traversal of all nodes under the first uip,
* clause extraction.

## Node coloring ##

A traversal of the conflict graph annotates nodes with the number of kids
they have. If `v` has `n` kids then `n` variable depend directly on the
value of `v` for their own.
The point of this step is to prevent the next one, uip extraction, from
considering the ancestors of a node if it has not dealt with all its
kids already.

The kid count of each node is stored in the variable map of the instance
and is reset during propagation.

## UIP extraction ##

This phase traverses the conflict graph and maintains a list `l` of nodes
to visit originally containing the conflict node.
An iteration begins by popping a node to visit, and decrease its kid
count. If its count is now `0` then all its kids were visited if `l` is
empty then all paths from the conflict node go to `n`, *i.e.* `n` is the
first uip.

If `l` is not empty then the clause which decided the value of `n` during
propagation is analyzed. The variables it mentions which are decisions /
propagations at level lower than `lvl` (peripheral nodes) are remembered.
Otherwise we add `n` to the list of nodes to visit.

This step terminates when it finds the first uip, which is guaranteed to
exist since the decision variable at the current decision level is a uip.

## Clause extraction ##

This last step takes the first uip and the peripheral nodes found by UIP
extraction to construct the clause to learn and the level to backjump to.
It ignores the decisions and propagations at level `0` since those are
intrinsic consequences of the CNF itself. The value of UIP is switched
and it is returned, along with the backjump level.
"]

use std::fmt ;
use std::path::Path ;

use ::conf::Conf ;
use ::common::* ;
use ::restart::{RestartStrat, Luby} ;
use ::instance::Instance ;
use ::dpll::* ;


/// Structure for a solver instance with cdcl by first uip.
///
/// See [module level documentation][index].
/// [index]: index.html (Module level documentation.)
pub struct Solver {
  /// Restart strategy of the solver.
  restart_strat: RestartStrat<Luby>,
  /// Actual instance.
  instance: Instance,
  /// Vector of previous decisions.
  decisions: Vec<Var>,
}

impl Solver {
  /// Creates a new satire solver instance.
  pub fn new(instance: Instance) -> Solver {
    // Decision level `0` is singleton clauses, `1` is assumptions. Thus max
    // size is variable count + 2, and decision level starts at 2.
    let mut decisions = Vec::with_capacity(instance.var_count() + 2) ;
    decisions.push(0) ; decisions.push(0) ;
    Solver {
      restart_strat: RestartStrat::<Luby>::luby(),
      instance: instance, decisions: decisions
    }
  }

  /// Creates a new satire solver instance from a file.
  pub fn of_path(
    path: & Path, conf: & Conf
  ) -> Result<Solver, & 'static str> {
    match Instance::of_path(path, conf) {
      Ok(instance) => Ok(Solver::new(instance)),
      Err(s) => Err(s),
    }
  }

  /// The statistics of the solver.
  pub fn stats(& self) -> & Statistics { & self.instance.stats }

  /// Colors the nodes of the current conflict graph by setting the
  /// `conflict_kids` field of nodes. Also, fills `periphery` will all the
  /// peripheral nodes.
  ///
  /// Does not color the decision at current level. If first uip reaches it,
  /// then it is the first uip and the lemma is the periphery.
  ///
  /// **Assumes** the `conflict_kids` field of all nodes is zero.
  #[inline]
  fn color_conflict_graph(
    & mut self, lvl: Lvl, clause: CIndex, _prefix: & str
  ) -> VarSet {

    // Periphery of the conflict graph.
    let mut periphery = VarSet::new(self.instance.var_count() / 2) ;
    // List of nodes to visit.
    let mut to_visit = Vec::new() ;
    // Filling it with the variables appearing in the conflict clause.
    log3!("{}| variables to visit from conflict clause:", _prefix) ;
    for lit in self.instance[clause].iter() {
      let var = lit.to_var() ;
      log3!("{}| {}", _prefix, var) ;
      to_visit.push(var)
    } ;

    log3!("{}| starting loop", _prefix) ;
    loop {
      if let Some( var ) = to_visit.pop() {

        log3!("{} \\ {}", _prefix, var) ;
        self.instance.inc_conflict_kids_of(var) ;

        if self.instance.conflict_kids_of(var) > 1 {
          log3!(
            "{}  | {} ({} kids) is known, skipping",
            _prefix, var, self.instance.conflict_kids_of(var)
          ) ;
          continue
        } ;

        log3!(
          "{}  | {} ({} kids) is new, analyzing source",
          _prefix, var, self.instance.conflict_kids_of(var)
        ) ;

        match self.instance.source_of_var(var) {
          Source::Decision(l) => {
            log3!("{}   \\ d@{}", _prefix, l) ;
            if l < lvl {
              log3!("{}    | peripheral", _prefix) ;
              periphery.add(var)
            } else {
              log3!("{}    | top", _prefix)
            }
          },
          Source::Propagation(l,_v,c) => {
            log3!(
              "{}   \\ p@{} from {} by {}: {}",
              _prefix, l, _v, c, self.instance[c]
            ) ;
            if l < lvl {
              log3!("{}    | peripheral", _prefix) ;
              periphery.add(var)
            } else {
              for lit in self.instance[c].iter() {
                let v = lit.to_var() ;
                log3!("{}    | appending", _prefix) ;
                if var != v { to_visit.push(v) }
              }
            }
          },
        }
      } else { break }
    }
    periphery
  }

  /// Finds the first uip in a colored conflict graph.
  #[inline]
  fn first_uip(
    & mut self, lvl: Lvl, clause: CIndex, periphery: VarSet, _prefix: & str
  ) -> (Var, VarSet) {

    // List of nodes to visit.
    let mut to_visit = Vec::new() ;
    let mut temp = Vec::new() ;
    // Contains the nodes for which we have yet to see all the kids.
    let mut pending = VarSet::new(self.instance.var_count() / 2) ;
    let mut uip_periphery = VarSet::new(self.instance.var_count() / 2) ;

    to_visit.push( (0, clause) ) ;

    loop {

      if let Some( (var, clause) ) = to_visit.pop() {
        log3!("{}| {}", _prefix, self.instance[clause]) ;

        if var > 0 && to_visit.is_empty() && pending.is_empty() {
          return (var, uip_periphery)
        } ;

        for lit in self.instance[clause].iter() {
          let v = lit.to_var() ;
          if v != var {
            match self.instance.source_of_var(v) {
              Source::Decision(l) => {
                log3!("{} \\ {}@d{}", _prefix, v, l) ;
                if l < lvl {
                  log3!("{}  | peripheral node", _prefix) ;
                  uip_periphery.add(v)
                } else {
                  log3!("{}  | current decision", _prefix) ;
                  return (v, periphery)
                }
              },
              Source::Propagation(l, _var, clause) => {
                log3!(
                  "{} \\ {}@p{} from {} by {}: {}",
                  _prefix, v, l, _var, clause, self.instance[clause]
                ) ;
                if l < lvl {
                  log3!("{}  | peripheral node", _prefix) ;
                  uip_periphery.add(v)
                } else {
                  if self.instance.conflict_kids_of(v) > 1 {
                    log3!(
                      "{}  | has {} kids, pending",
                      _prefix, self.instance.conflict_kids_of(v)
                    ) ;
                    temp.push(v)
                  } else {
                    log3!(
                      "{}  | has no more kids, appending", _prefix
                    ) ;
                    pending.rm(v) ;
                    to_visit.push( (v, clause) )
                  }
                }
              },
            }
          }
        } ;

        loop {
          if let Some( var ) = temp.pop() {
            self.instance.dec_conflict_kids_of(var) ;
            pending.add(var)
          } else { break }
        }

      } else {
        panic!("[first uip] could not find uip")
      }

    }

  }
}

impl fmt::Display for Solver {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    let latest = self.decisions[0] ;
    try!( write!(
      fmt, "{{ {}{}",
      match self.instance.val_of_var(latest) {
        Some(true) => "", Some(false) => "-",
        None => panic!("unassigned decision variable {}", latest),
      },
      latest
    ) ) ;
    for var in 1..(self.decisions.len() - 1) {
      try!( write!(
        fmt, ",{}{}",
        match self.instance.val_of_var(var) {
          Some(true) => "", Some(false) => "-",
          None => panic!("unassigned decision variable {}", var),
        },
        var
      ) )
    } ;
    write!(fmt, " | {} }}", self.instance)
  }
}

impl HasInstance for Solver {
  fn instance(& mut self) -> & mut Instance {
    & mut self.instance
  }
}


impl Dpll for Solver {

  fn is_model(& self) -> bool { self.instance.is_model() }

  fn print_valuations(& self, prefix: & str) {
    self.instance.print_valuations(prefix)
  }

  #[inline]
  fn lvl(& self) -> Lvl { self.decisions.len() - 1 }

  #[inline]
  fn dec_var(& self) -> Var {
    self.decisions[self.lvl()]
  }

  #[inline]
  fn dec_var_of(& self, lvl: Lvl) -> Var {
    self.decisions[lvl]
  }

  fn decide(& mut self) -> Option<Var> {
    match self.instance.pop_heaviest_var() {
      None => None,
      Some(var) => {
        assertion!(eq self.instance[var].value, None) ;
        self.instance.stats.decisions_inc() ;
        self.decisions.push(var) ;
        // let val = self.instance[var].hint ;
        let val = Valuation {
          val: true, from: Source::Decision(self.lvl())
        } ;
        self.instance.mut_info_of(var).value = Some(val) ;
        Some( var )
      }
    }
  }

  fn propagate(& mut self, lvl: Lvl, var: Var) -> Option<(Var,CIndex)> {
    self.instance.propagate(lvl, var)
  }

  fn backjump(& mut self, lvl: Lvl, var: Var) -> Option<Var> {
    // logln!("backjumping to {}", lvl) ;
    // Cannot backtrack assumptions or singleton clauses.
    if lvl < 1 { None } else {

      // Backjumping to lvl 1 is the same as a restart, so we only check
      // for restarts when above.
      if lvl > 1 {
        if self.restart_strat.should_restart() {
          // If we should restart backjump to 1 and make a decision.
          // logln!("restarting") ;
          // self.instance.print_variables_weight("  ") ;
          self.instance.stats.restarts_inc() ;
          while self.lvl() > 1 { self.decisions.pop() ; } ;
          self.instance.backjump(1) ;
          self.instance.normalize_var_weight() ;
          // logln!("weights after:") ;
          // self.instance.print_variables_weight("  ") ;
          return self.decide()
        }
      } ;

      // Otherwise backjump and return variable.
      while self.lvl() > lvl { self.decisions.pop() ; } ;
      self.instance.backjump(lvl) ;
      // logln!("returning some({})", var) ;
      Some(var)
    }
  }

  fn resolve(
    & mut self, lvl: Lvl, _: Var, clause: CIndex
  ) -> (Lvl, Var) {

    use ::common::Source::* ;
    use std::cmp::max ;

    self.instance.stats.conflicts_inc() ;

    log2!(
      "  | resolving conflict at {} for {}: {}",
      lvl, clause, self.instance[clause]
    ) ;

    log2!("   \\ coloring graph") ;
    let periphery = self.color_conflict_graph(lvl, clause, "    ") ;
    log2!("    | done, periphery is {}", periphery) ;

    log2!("   \\ extracting first uip") ;
    let (uip, uip_periphery) = self.first_uip(lvl, clause, periphery, "    ") ;
    log2!(
      "    | done, uip is {} with periphery {}", uip, uip_periphery
    ) ;

    log2!("   \\ constructing lemma") ;

    let mut lemma = vec![] ;
    let mut level = 0 ;
    let mut from = (0 as Lvl, None) ;

    lemma.push( self.instance.neg_lit_of_var(uip) ) ;

    if uip_periphery.is_empty() {
      log2!("    | uip periphery is empty") ;
      // Need to backtrack to the level before this one.
      level = self.instance.lvl_of_var(uip) - 1 ;
      // Source for the uip will be decision at 0. No update to `from`.
    } else {
      log2!("    | analyzing periphery") ;
      for var in uip_periphery.iter() {
        let var = * var ;
        let lvl = self.instance.lvl_of_var(var) ;
        log2!("    | {}@{}", var, lvl) ;
        if lvl > 1 {
          level = max(level, lvl) ;
          lemma.push( self.instance.neg_lit_of_var(var) )
        } ;
        if level == lvl { from = (lvl, Some(var))  }
      } ;

      if level == 0 {
        // New value of uip is a consequence of things at 0.
        // Need to backtrack to 1.
        level = 1 ;
        // From was updated in the loop above.
      }
    } ;

    let source = match from {
      (lvl, None) => {
        assertion!( eq lemma.len(), 1 ) ;
        // let _ = self.instance.add_learnt(uip, lemma) ;
        log2!( "    | no clause learnt" ) ;
        Source::Decision(lvl)
      },
      (lvl, Some(var)) => {
        let cindex = self.instance.add_learnt(uip, lemma) ;
        log2!(
          "    |  clause learnt{} ({})", self.instance[cindex], cindex
        ) ;
        Source::Propagation(lvl, var, cindex)
      },
    } ;

    log2!(
      "    | backjump level is {}, source is {}", level, source
    ) ;
    self.instance.switch(uip, source) ;

    ( level, uip )
  }

  fn print(& self) {
    log!("  {}", self.instance) ;
    if self.decisions.len() > 1 {
      let latest = self.decisions[self.decisions.len() - 1] ;
      log!( nlb,
        "  {}{}",
        match self.instance.val_of_var(latest) {
          Some(true) => "+", Some(false) => "-", None => "!",
        },
        latest
      ) ;
      for var in self.decisions.iter().rev().skip(1) {
        let var = * var ;
        if var != 0 {
          print!(
            ",{}{}",
            match self.instance.val_of_var(var) {
              Some(true) => "+", Some(false) => "-", None => "!",
            },
            var
          )
        }
      } ;
      println!("")
    }
  }
}



#[cfg(any(bench))]
use test::Bencher ;
#[cfg(any(test, bench))]
use ::common::SolverRes::{Sat, Unsat, Timeout} ;

#[cfg(any(test, bench))]
pub fn run_test(path: & Path, conf: & Conf, res: SolverRes ) {
  use ::dpll::Dpll ;
  let path_str = path.to_str().unwrap() ;
  match Solver::of_path( path, & conf ) {
    Err(s) => panic!("[{}, {}] dpll creation: {}", path_str, res, s),
    Ok(mut solver) => {
      let solver = & mut solver ;
      let got = solver.solve(vec![]) ;
      match got {
        Timeout => panic!(
            "[{}, {}] got timeout", path_str, res
        ),
        _ => {
          if got == Sat {
            if ! solver.is_model() {
              panic!(
                "[{}, {}] got sat but could not confirm model", path_str, res
              )
            }
          } ;
          if got != res {
            panic!("[{}, {}] got {}", path_str, res, got)
          }
        }
      }
    },
  }
}

#[cfg(any(test, bench))]
pub fn run_test_dir(path_str: & str) {
  use std::fs::read_dir ;
  use std::ffi::OsStr ;
  let files = read_dir(& Path::new(path_str)).unwrap() ;
  let mut conf = Conf::default_conf() ;
  conf.set_timeout(20) ;
  let cnf = OsStr::new("cnf") ;
  for file in files {
    let path = file.unwrap().path() ;
    let path = path.as_path() ;
    match (path.file_stem(), path.extension()) {
      (Some(file_stem), Some(ext)) if ext == cnf => {
        let file_stem = file_stem.to_str().unwrap() ;
        let expected = if file_stem.starts_with("uf") {
          Sat
        } else {
          if file_stem.starts_with("uuf") {
            Unsat
          } else { panic!(
              "[{}] cnf file does not start with \"uf\" or \"uuf\"", path_str
          ) }
        } ;
        run_test(path, & conf, expected)
      },
      _ => (),
    }
  }
}




#[test]
fn regression() { run_test_dir("rsc/regression/") }
#[test]
fn sat_20() { run_test_dir("rsc/uf20/") }
#[test]
fn sat_50() { run_test_dir("rsc/uf50/") }
#[test]
fn unsat_50() { run_test_dir("rsc/uuf50/") }
#[test]
fn sat_75() { run_test_dir("rsc/uf75/") }
#[test]
fn unsat_75() { run_test_dir("rsc/uuf75/") }
#[test]
fn sat_100() { run_test_dir("rsc/uf100/") }
#[test]
fn unsat_100() { run_test_dir("rsc/uuf100/") }
// #[test]
// fn sat_125() { run_test_dir("rsc/uf125/") }
// #[test]
// fn unsat_125() { run_test_dir("rsc/uuf125/") }

// #[bench]
// fn regression_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/regression/") )
// }
// #[bench]
// fn sat_20_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/uf20/") )
// }
// #[bench]
// fn sat_50_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/uf50/") )
// }
// #[bench]
// fn unsat_50_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/uuf50/") )
// }
// #[bench]
// fn sat_75_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/uf75/") )
// }
// #[bench]
// fn unsat_75_bench(bench: & mut Bencher) {
//   bench.iter(|| run_test_dir("rsc/uuf75/") )
// }