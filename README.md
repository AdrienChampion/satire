# SatIre #

> *"Unsatisfiable here, but maybe satire."* [(hint)][lame joke hint]

SatIre is a SAT solver written in Rust.

## Build ##

Building follows the usual cargo workflow.

To build the debug version to `target/debug/satire`:

```
cargo build
```

To check that all test pass:

```
cargo test
```

It can take a while, to run top level tests only:

```
cargo test --solver
```

To build the release version to `target/release/satire`:

```
cargo build --release
```



### Build features ###

The activation of logging and/or assertions is done at compile time using
**features** in cargo. The features available are

|            |          |                                      |
|------------|:---------|:-------------------------------------|
| Log        | `log1`   | log level 1                          |
|            | `log2`   | log level 2, activates level 1       |
|            | `log3`   | log level 3, activates level 1 and 2 |
|            |          |                                      |
| Assertions | `assert` | activates sanity checks              |

For example,

```
cargo build --release --features "log2 assert"
cargo build --release --features "log1"
```



### Rust / Cargo ###

To install the latest version of rust and cargo:

```bash
curl -sS https://static.rust-lang.org/rustup.sh > install.sh
cat install.sh
chmod u+x install.sh
./install.sh
```


[lame joke hint]: http://dictionary.reverso.net/french-english/ailleurs (Lame joke hint.)