#!/bin/bash

function sep_line {
  echo "    ----------------------------------"
}
echo ""

# Creates the info.rs file used by satire to know its version and git hash.
git_hash=`git rev-parse HEAD`
git_hash_tag="<|git_hash|>"
echo "          $git_hash_tag $git_hash"

git_short_hash=`git rev-parse --short HEAD`
git_short_hash_tag="<|git_short_hash|>"
echo "    $git_short_hash_tag $git_short_hash"

git_branch=`git branch | grep -e "*" | sed -e 's:*::' | tr -d ' '`
git_branch_tag="<|git_branch|>"
echo "        $git_branch_tag $git_branch"

git_modified=`git ls-files -m | grep -e "src" | sed -e 's:.*src/\(.*\):\1:'`
git_modified=`echo "$git_modified" | tr -d ' '`
git_modified_count=`echo "$git_modified" | wc -l | tr -d ' '`
git_modified_count_tag="<|git_modified_count|>"
echo "$git_modified_count_tag $git_modified_count"

git_modified=`echo $git_modified | sed -e 's: :", ":g'`
git_modified=`echo $git_modified | sed -e 's:^\(.*\)$:["\1"]:'`
git_modified_tag="<|git_modified|>"
echo "      $git_modified_tag $git_modified"

sep_line

satire_version=`cat Cargo.toml | grep -e "version = "`
satire_version=`echo $satire_version | sed -e 's:.*"\(.*\)"$:\1:'`
satire_version_tag="<|version|>"
echo "$satire_version_tag $satire_version"


skel_file="./scripts/skel/info.rs"
target_dir="./src/"
target_file="${target_dir}info.rs"

if [ -f "$skel_file" ]
then
  if [ -d "$target_dir" ]
  then
    echo "Writing \"$target_file\"."
    eval "cat $skel_file | sed \
      -e 's:$git_hash_tag:$git_hash:' \
      -e 's:$git_short_hash_tag:$git_short_hash:' \
      -e 's:$git_branch_tag:$git_branch:' \
      -e 's:$git_modified_tag:$git_modified:' \
      -e 's:$git_modified_count_tag:$git_modified_count:' \
      -e 's:$satire_version_tag:$satire_version:' > $target_file"
    echo "Done."
  else
    echo ""
    >&2 echo "[Error] Could not locate src directory \"$target_dir\"."
    echo ""
    exit 2
  fi
else
  echo ""
  >&2 echo "[Error] Could not locate skeleton file \"$skel_file\"."
  echo ""
  exit 2
fi

echo ""
exit 0