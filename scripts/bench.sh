#!/bin/bash

now=`date`
sed_cmd="gsed"
time_cmd="gtime"
res_dir="./"
verbose="1"
format="pdf"

function print_usage {
  cat <<EOF

Usage: `basename $0` [<option>]* [-<id> "<cmd>"]+ <bench-file>
Runs some sat solvers on some benchmarks.
where
  <bench-file> is a file the benchmark files to run the solvers on.
               Empty lines and those starting with "%" are ignored.
  [-<id> "<to-run>"] corresponds to a solver to run:
     * <id> is the solver identifier and MUST BE A LEGAL FILENAME,
     * <to-run> is the command to run.
and <option> can be
  [--help]      prints this message and exits.
  [--v <int>]   sets the verbose level [$verbose] (capped to 2).
  [--out <dir>] sets the output directory for the result [$res_dir].

EOF
}

function print_error {
  print_usage
  echo "Error: $1"
  echo ""
  exit -1
}

function cursor_go_up {
  if [ "$verbose" -eq 1 ]
  then
    echo -en "\033[1A"
  fi
}
function cursor_clean_line {
  if [ "$verbose" -eq 1 ]
  then
    echo -en "\033[K"
  fi
}
function cursor_line_start {
  if [ "$verbose" -eq 1 ]
  then
    echo -en "\033[1000D"
  fi
}

solver_id_array=()
solver_log_file_array=()
solver_cmd_array=()
let "solver_last_index=-1"

function inc_solver_last_index {
  let "solver_last_index=solver_last_index+1"
}

function unquote {
  result=`echo $1 | $sed_cmd -e "s:^\"::" -e "s:\"$::"`
}

function insert_solver {
  id_temp=`echo $1 | $sed_cmd -e "s:^-::"`
  unquote "$2"
  cmd_temp="$result"
  inc_solver_last_index
  # echo "Inserting [id=$id_temp, cmd=$cmd_temp] at $solver_last_index."
  solver_id_array[$solver_last_index]=$id_temp
  solver_cmd_array[$solver_last_index]=$cmd_temp
}

function print_solver_arrays {
  if [ "$verbose" -gt 0 ]
  then
    echo "Solvers:"
    for i in $(seq 0 $solver_last_index)
    do
      echo "  ${solver_id_array[$i]}"
      echo "  > command:  \"${solver_cmd_array[$i]}\""
      echo "  > log file: \"${solver_log_file_array[$i]}\""
    done
  fi
}

function verb_one {
  if [ "$verbose" -ge 1 ]
  then
    echo "$1"
  fi
}

function verb_two {
  if [ "$verbose" -ge 2 ]
  then
    echo "$1"
  fi
}

function print_bench_explanation_to {
  cat >> "$1" <<-EOF
#
# Statistics obtained using the "time" command:
# - wall_clock_time is "%E"
# - cpu_time is the sum of "%U" and "%S"
# - max_mem is "%M":
#   "Maximum resident set size of the process during its lifetime, in Kbytes."
# - avrg_mem is "%K":
#   "Average total (data+stack+text) memory use of the process, in Kbytes."
#
# file     wall_clock_time     cpu_time     max_mem     avrg_mem
#
EOF
}

function print_plot {
  in_file="$1"
  out_file="$2"
  text="$3"
  id1="$4"
  id2="$5"
  columns="$6"
  plot_dir="$7"
  max_val="$8"
  cat > "${plot_dir}${out_file}.plot" <<EOF
# $id1 against $id2:
# > $text
#
set term $format
set logscale x
set logscale y
set size square
set output "$out_file.$format"
set title "$text ($id1/ $id2)"
set xlabel "$id1"
set ylabel "$id2"
set xrange [0.009:$max_val]
set yrange [0.009:$max_val]
plot "$in_file" using $columns notitle, x notitle
EOF
  gnuplot <<EOF
set term $format
set logscale x
set logscale y
set size square
set output "${out_dir}$out_file.$format"
set title "$text ($id1/ $id2)"
set xlabel "$id1"
set ylabel "$id2"
set xrange [0.009:$max_val]
set yrange [0.009:$max_val]
plot "$in_file" using $columns notitle, x notitle
EOF
}

function print_plots {
  in_file="$1"
  out_dir="$2"
  id1="$3"
  id2="$4"
  plot_dir="$5"
  max_things=`cat "$in_file" | awk '{ \
    if(m2==""){m2=m3=m4=m5=0} ; \
    if(m2 < $2){m2=$2} ; if(m2 < $6){m2=$6} ; \
    if(m3 < $3){m3=$3} ; if(m3 < $7){m3=$7} ; \
    if(m4 < $4){m4=$4} ; if(m4 < $8){m4=$8} ; \
    if(m5 < $5){m5=$5} ; if(m5 < $9){m5=$9} ; \
  }END{ \
    m2=m2+m2/2 ; \
    m3=m3+m3/2 ; \
    m4=m4+m4/2 ; \
    m5=m5+m5/2 ; \
    print m2":"m3":"m4":"m5 \
  }'`
  max_things=(${max_things//:/ })
  out_file="time-wc_$id1-$id2"
  text="Wall clock time"
  print_plot "$in_file" "$out_file" "$text" \
             "$id1" "$id2" "2:6" "$plot_dir" "${max_things[0]}"
  out_file="time-cpu_$id1-$id2"
  text="Cpu time"
  print_plot "$in_file" "$out_file" "$text" \
             "$id1" "$id2" "3:7" "$plot_dir" "${max_things[1]}"
  out_file="mem-max_$id1-$id2"
  text="Max memory"
  print_plot "$in_file" "$out_file" "$text" \
             "$id1" "$id2" "4:8" "$plot_dir" "${max_things[2]}"
  out_file="mem-avg_$id1-$id2"
  text="Average memory"
  print_plot "$in_file" "$out_file" "$text" \
             "$id1" "$id2" "5:9" "$plot_dir" "${max_things[3]}"
}

function make_solver_log_files {
  # Check that all files can be created first.
  for i in $(seq 0 $solver_last_index)
  do
    id="${solver_id_array[$i]}"
    log_file="${res_dir}${id}.res"
    touch "$log_file"
    if [ "$?" -ne 0 ]
    then
      print_error "cannot create log file \"$log_file\" for $id"
    fi
    rm "$log_file"
  done
  # Then actually create and log them
  for i in $(seq 0 $solver_last_index)
  do
    id="${solver_id_array[$i]}"
    cmd="${solver_cmd_array[$i]}"
    log_file="${res_dir}${id}.res"
    touch "$log_file"
    solver_log_file_array[$i]=$log_file
  done
}

function make_plots {
  join_dir="${res_dir}joins/"
  mkdir -p "$join_dir"
  plot_dir="${res_dir}plots/"
  mkdir -p "$plot_dir"
  let "last_index_m1=solver_last_index-1"
  for i in $(seq 0 $last_index_m1)
  do
    let "ip1=i+1"
    for j in $(seq $ip1 $solver_last_index)
    do
      file1="${solver_log_file_array[$i]}"
      file2="${solver_log_file_array[$j]}"
      id1="${solver_id_array[$i]}"
      id2="${solver_id_array[$j]}"
      join_file="${join_dir}${id1}-${id2}.join"
      join "$file1" "$file2" > "${join_file}"
      print_plots "$join_file" "$res_dir" "$id1" "$id2" "$plot_dir"
    done
  done
}

function finish_this_shit {
  make_plots
  data_dir="${res_dir}data/"
  mkdir -p "$data_dir"
  for i in $(seq 0 $solver_last_index)
  do
    id="${solver_id_array[$i]}"
    cmd="${solver_cmd_array[$i]}"
    log_file="${solver_log_file_array[$i]}"
    new_log_file="${data_dir}${id}.res"
    touch "$new_log_file"
    echo "# Results for run on $now" >> $new_log_file
    echo "# > solver id | $id" >> $new_log_file
    echo "# > command   | $cmd" >> $new_log_file
    print_bench_explanation_to $new_log_file
    cat "$log_file" >> "$new_log_file"
    rm "$log_file"
  done
}

if [ $# -lt 2 ]
then
  print_error "no benchmark directory specified"
fi

while :
do
  case $1 in
    --help)
      print_usage
      exit 0
      ;;
    --v)
      shift
      verbose="$1"
      shift
      ;;
    --out)
      shift
      res_dir=`echo $1 | $sed_cmd -e "s://*$::"`
      res_dir="${res_dir}/"
      shift
      ;;
    -*)
      insert_solver "$1" "$2"
      shift
      shift
      ;;
    *)
      bench_file=$1
      break
      ;;
  esac
done

if [ "$bench_file" = "" ]
then
  print_error "no benchmark file specified"
fi

if [ ! -f $bench_file ]
then
  print_error "benchmark file does not exist"
fi

benchmarks=`cat $bench_file | grep -v -e "^%" -e "^[[:space:]\t]*$" -e "^[[:space:]\t]*%"`
bench_count=`echo "$benchmarks" | wc -l | tr -d ' '`

if [ "$benchmarks" = "" ]
then
  print_error "no file found in [$bench_file]"
fi

if [ "$res_dir" = "" ]
then
  print_error "result directory is undefined"
fi

if [ ${#solver_id_array[@]} -lt 1 ]
then
  print_error "no solver to run"
fi

int_re='^[0-9]+$'
if ! [[ $verbose =~ $int_re ]] ; then
   print_error "verbose level is not an integer"
fi

make_solver_log_files

error_file="${res_dir}/errors"
touch "$error_file"
rm "$error_file"
touch "$error_file"

verb_one ""
verb_one "Running on $bench_count benchmarks, logging to \"$res_dir\"."
print_solver_arrays
verb_one ""
sed_fix_zeros="-e 's:\( \|^\)\([0]\+[\.[0]*\|]\)\( \|$\):\10.01\3:g'"
# Twice for trailing instances.
sed_fix_zeros="$sed_cmd $sed_fix_zeros $sed_fix_zeros"

function status_to_file {
  case $1 in
    0)
      printf "<timeout>" >> $2
      ;;
    10)
      printf "sat" >> $2
      ;;
    20)
      printf "unsat" >> $2
      ;;
    *)
      printf "<$1>" >> $2
      ;;
  esac
}

for file in `echo "$benchmarks"`
do
  if [ -f $file ]
  then
    verb_one "Running on $file"
    expected_exit_status="0"
    let "errors=0"
    for i in $(seq 0 $solver_last_index)
    do
      id="${solver_id_array[$i]}"
      cmd="${solver_cmd_array[$i]}"
      log_file="${solver_log_file_array[$i]}"
      to_run="$cmd $file"
      verb_one "> $id | $to_run"
      output=`($time_cmd -f ">%e %U %S %M %K" $to_run) 2>&1 1> /dev/null`
      exit_status="$?"
      verb_two "  | $exit_status"
      output=`echo "$output" | grep -e "^>" | $sed_cmd -e "s:>::"`
      output=`echo "$output" | awk '{cpu=$3+$4; print $1,$2,cpu,$5}'`
      output=`echo "$output" | eval "$sed_fix_zeros"`
      if [ "$exit_status" -ne 0 ] && [ "$exit_status" -ne 10 ] && [ "$exit_status" -ne 20 ]
      then
        echo "[$file] $id returned with exit code $exit_status" >> $error_file
      elif [ "$expected_exit_status" -ne "$exit_status" ] && [ "$expected_exit_status" -ne 0 ] && [ "$exit_status" -ne 0 ]
      then
        let "errors=errors+1"
        printf "[$file] $id says " >> $error_file
        status_to_file "$exit_status" "$error_file"
        printf " but previous solver(s) said " >> $error_file
        status_to_file "$expected_exit_status" "$error_file"
        echo "" >> "$error_file"
      fi
      expected_exit_status="$exit_status"
      verb_two "  | $output"
      echo "$file $output" >> $log_file
      cursor_go_up
      cursor_line_start
      cursor_clean_line
    done
    cursor_go_up
    cursor_clean_line
    if [ "$errors" -le 0 ]
    then
      verb_one "Done for $file"
    elif [ "$errors" -le 1 ]
    then
      verb_one "Done for $file (1 error)"
    else
      verb_one "Done for $file ($errors errors)"
    fi
  else
    verb_one "Error: file \"$file\" does not exists."
  fi
done

finish_this_shit

error_count=`cat $error_file | wc -l | tr -d ' '`
if [ "$error_count" -ne 0 ]
then
  echo ""
  echo "Error: $error_count failure(s) detected."
  echo "Error file is \"$error_file\":"
  echo
  cat "$error_file"
  echo
  exit 2
else
  rm "$error_file"
  verb_one "Done, no errors."
fi

verb_one