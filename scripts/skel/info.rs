#![ doc = "
"]

/// Git commit hash:
/// `<|git_hash|>`.
pub static git_hash: & 'static str = "<|git_hash|>" ;

/// Git short commit hash:
/// `<|git_short_hash|>`.
pub static git_short_hash: & 'static str = "<|git_short_hash|>" ;

/// Git branch:
/// `<|git_branch|>`.
pub static git_branch: & 'static str = "<|git_branch|>" ;

/// The files modified from the git hash, if any:
/// `<|git_modified|>`.
pub static git_modified: [
  & 'static str ; <|git_modified_count|>
] = <|git_modified|> ;

/// The version of satire as documented in `Cargo.toml`:
/// `<|version|>`.
pub static version: & 'static str = "<|version|>" ;

