#[ doc = "Build script for satire. Calls `prepare.sh`" ]

fn main() {
  let program = "./scripts/prepare.sh" ;
  // panic!("Running \"{}\"", program) ;
  let mut command = std::process::Command::new(program) ;
  let status = command.status().unwrap_or_else(
    |e| panic!("could not run build script \"{}\": {}", program, e)
  ) ;
  if ! status.success() {
    panic!("build script did not exit successfully")
  }
}