#!/bin/bash

timeout="100"
verbose="1"
now=`date +"%Y-%m-%d_%Hh%M"`
git_hash=`git log --pretty=format:'%h' -1`
res_dir_default="benchs/${now}_${git_hash}/"
res_dir="$res_dir_default"
bench_script="scripts/bench.sh"
bench_file="rsc/bench.files"
sed_cmd="gsed"

function print_usage {
  cat <<EOF

Usage: `basename $0` [<option>]* [<bench-file>]
Runs satire and lingeling on some benchmarks (using scripts/bench.sh).
where
  <bench-file> is a file the benchmark files to run the solvers on.
               Empty lines and those starting with "%" are ignored.
               Actually optional, default is \"$bench_file\".
and <option> can be
  [--help]      prints this message and exits.
  [--to <int>]  sets the timeout [$timeout].
  [--v <int>]   sets the verbose level [$verbose] (capped to 2).
  [--out <dir>] sets the output directory for the result.
                Default is <date>_<time>_<git_hash> [$res_dir_default].

EOF
}

function print_error {
  print_usage
  echo "Error: $1"
  echo ""
  exit -1
}

while :
do
  case $1 in
    --help)
      print_usage
      exit 0
      ;;
    --to)
      shift
      timeout="$1"
      shift
      ;;
    --v)
      shift
      verbose="$1"
      shift
      ;;
    --out)
      shift
      res_dir=`echo $1 | $sed_cmd -e "s://*$::"`
      res_dir="${res_dir}/"
      shift
      ;;
    -*)
      insert_solver "$1" "$2"
      shift
      shift
      ;;
    *)
      if [ "$1" != "" ]
      then
        bench_file=$1
      fi
      break
      ;;
  esac
done

int_re='^[0-9]+$'
if ! [[ $timeout =~ $int_re ]]
then
  print_error "timeout is not an integer"
fi

if [ ! -f "$bench_script" ]
then
  print_error "could not find bench script \"$bench_script\""
fi

if [ "$res_dir_default" == "$res_dir" ]
then
  mkdir -p "$res_dir"
fi

lingeling_cmd="lingeling -t $timeout"
satire_cmd="satire -to $timeout"

to_run="--v $verbose --out $res_dir -lingeling \"$lingeling_cmd\" -satire \"$satire_cmd\" $bench_file"

echo "$bench_script $to_run"
eval "$bench_script $to_run"
