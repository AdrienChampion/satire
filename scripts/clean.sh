#!/bin/bash

for file in `find rsc/ -iname "*.cnf"` ; do
  echo "file: $file"
  gsed -e 's:^%$::' -e 's:^0$::' $file > temp.temp
  mv temp.temp $file
done